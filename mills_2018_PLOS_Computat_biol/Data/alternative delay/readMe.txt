Robin Mills 2018
This is the output of the simulations for "Physics-based simulations of aerial attacks by peregrine falcons reveal that stooping at high speed maximizes catch success against agile prey"
Specifically, this data reproduces supplementary figure 3.

The files are named as follows:
preyManeuver_reactionTimePredator_visualErrorPredator_controlErrorPredator

Prey maneuver indices denote the following maneuvers:
1. linear
2. non-smooth
3. smooth

Reaction time is indicated in seconds, visual error in radians*1000, and control error is indicated in as a proportion. 
To re-generate the data found on this gitlab repository, run the simulation model under the desired settings, and run this script to generate a GAM-smoothed grid. The grid is used to generate the plots of the paper. 
To generate the plots, see script "step_2.output.R"
