function [ p ] = plot_level_acceleration(figureNumber, birds_morph, birds, style)
% plots the level acceleration versus airspeed for all birds
    lWidth = 3;
    mSize = 100;
    labelSize = 1;
   
    if style == 1; fig = figure(figureNumber); else
        fig = figure(1);
        subplot(3,2,figureNumber);
    end
    xSpan = 35;
    ySpan = 12;
     xlim([0 35]);
    ylim([0,12]);
    fig.RendererMode = 'manual';  % use a set with older versions of Matlab
    fig.Renderer = 'painters';
    hold on;
    num = 200;
    xlabel('airspeed (ms^{-1})', 'interpreter', 'Tex' );
    ylabel('level acceleration (ms^{-2})');
    
    plots = [];
    names = {};
    for n = 1:length(birds)
        the_bird = birds{n};
        the_bird_morph = birds_morph{n};
         if (ischar(the_bird_morph.plotColor)); pcolor = strcat('', the_bird_morph.plotColor); else; pcolor = the_bird_morph.plotColor; end
        plots(n) = plot(the_bird.airspeed,the_bird.max_forward_acc_level_flight, 'Color',pcolor, 'LineWidth', lWidth, 'MarkerSize', mSize);
        names{n} = [string(n){1} '. ' the_bird_morph.name{1}];
        
    end
    for n = 1:length(birds) 
        the_bird = birds{n};
        the_bird_morph = birds_morph{n};
        ySizeRectangle = ySpan ./ 35 .* labelSize;
        xSizeRectangle = xSpan ./ 35 .* labelSize;
        if (ischar(the_bird_morph.plotColor)); pcolor = strcat('', the_bird_morph.plotColor); else; pcolor = the_bird_morph.plotColor; end
        rectangle('Position',[the_bird.airspeed(1100+n.*150)-xSizeRectangle./2 the_bird.max_forward_acc_level_flight(1100+n.*150)-ySizeRectangle./2 xSizeRectangle ySizeRectangle],'Curvature',1, 'FaceColor',[1 1 1],'EdgeColor', pcolor, 'LineWidth',lWidth/2)
        tx = text(the_bird.airspeed(1100+n.*150),the_bird.max_forward_acc_level_flight(1100+n.*150), string(n){1});
        tx.HorizontalAlignment = 'center';
        tx.FontSize = 12;
    end
    set(gca,'XTick',0:4:400);
    grid on;
    xlim([0 35]);
    ylim([0,12]);
    title('(a)')
    p = fig;
    
    %leg = legend(plots,names)
end

