function [ output] = calculate_flight_performance( bird )
    
    %_________________setting variables____________________________________
    theta = bird.theta;
    f = bird.f;
    clsteady = bird.clsteady;
    LEV = bird.LEV;
    b = bird.b;
    wl = bird.wl;
    area = bird.area;
    density = bird.density;
    area_ = area - area*2*wl/b;
    %area = area*2*wl/b;
    AR = bird.AR;
    m = bird.m;
    g = bird.g;
    L = m*g;
    bodymass = bird.bodymass;
    wingmass = bird.wingmass;
    bodyArea = bird.bodyArea;
    cbody = bird.cbody;
    cfriction = bird.cfriction;
    InertiaBody = bird.InertiaBody;
    InertiaWing = bird.InertiaWing;
    InertiaJ = bird.InertiaJ;
    
    %___________________________________perform_calculations_______________
    %set airspeed
    v = 0:0.01:130;
    %with different stroke plane:
    v_adjusted = v - (0.5*f*pi*wl*sin(theta))^2;
    %calculate coefficient of lift for level flight
    cl = L ./ (0.5.*density.*area.*v.^2);
    %retrieve maximum coefficient of lift
    clmax = f^2 ./ (f^2 + v.^2) * LEV + clsteady;
    %constrain to maximum
    cl(cl>clmax) = clmax(cl>clmax);
    %calculate thrust - induced drag
    TDi_flapping_level = -cl.^2.*area.*density.*v.^2 ./ (2.*pi.*AR) + (1 - cl.^2./clmax.^2).*pi.^3 ./ 16 .* density .*AR.*area.*(sin(0.5.*theta).*f).^2 .* wl.^2;
    TDi_flapping_dive = pi.^3 ./ 16 .* density .*AR.*area.*(sin(0.5.*theta).*f).^2 .* wl.^2;
    %calculate body and friction drag
    D = cbody .* 0.5.*density.*bodyArea .* v.^2 + cfriction .* 0.5.*density.*area .* v.^2;
    %cruise speed
    cruisespeed = v(TDi_flapping_level-D == max(TDi_flapping_level-D));
    %correction factor for torque constraints
    c_torque = min(1,cruisespeed./v);
    %adjust TDi
    TDi_flapping_level = TDi_flapping_level.*c_torque;
    TDi_flapping_dive = TDi_flapping_dive.*c_torque;
    %calculate top speed in flapping level flight
    v_max_level = v(abs(TDi_flapping_level-D) == min(abs(TDi_flapping_level(v>cruisespeed)-D(v>cruisespeed))));
    %calculate top speed in dive
    v_max_dive = sqrt(9.81.*m ./ (cbody .* 0.5.*density.*bodyArea));
    % gliding with retracted wings to minimize drag
    bmax = b;
    bmin = b - 2*wl;
    Smax = area;
    %calculate wing retraction for optimal forward speed
    b =  ((8.*L.^2.*bmax - 8.*L.^2.*bmin)./(pi.*Smax.*cfriction.*density.^2.*v.^4)).^(1./3);
    %constrain cl and b
    b(b<bmin) = bmin;
    b(b>bmax) = bmax;
    cl_retracted = L ./ (0.5.*density.*Smax.*(b-bmin)./(bmax - bmin).*v.^2);
    cl_retracted(cl_retracted>clmax) = clmax(cl_retracted>clmax);
    b = L ./ (0.5.*density.*Smax.*cl_retracted./(bmax - bmin).*v.^2) + bmin;
    b(b>bmax) = bmax;
    %calculate wing retraction for maximizing lift
    b_maxlift = sqrt(((bmax-bmin).^2.*1.7.*m.*g)./(clmax.*density.*0.5.*Smax.*v.^2.)) + bmin ;
    b_maxlift(b_maxlift>bmax) = bmax;
    %corresponding wing area
    area_maxlift = Smax .* (b_maxlift-bmin)./(bmax - bmin);
    %calculate maximum lift for retracted wings
    L_max = clmax.*0.5.*density.*area_maxlift.*v.^2;
    %calculate area and aspect ratio for maximizing forward acceleration 
    area2 = Smax .* (b-bmin)./(bmax - bmin);
    AR2 = b.^2./area2;
    %calculate minimum drag while gliding
    D2 = cbody .* 0.5.*density.*bodyArea .* v.^2 + cfriction .* 0.5.*density.*area2 .* v.^2 + cl_retracted.^2.*area2.*density.*v.^2 ./ (2.*pi.*AR2);
    %calculate maximum forward acceleration at level flight (maximum of gliding and flapping)
    ACCmax = max(TDi_flapping_level-D,-D2) ./ m;
    %calculate maximum forward acceleration in vertical dive (maximum of gliding and flapping)
    ACCdive = max((TDi_flapping_dive - D),-D2) ./ m + 9.81;
    %calculate inertia
    phi = (b_maxlift - bmin) ./ (bmax - bmin);
    InertiaWingCenter = InertiaWing.*phi.^2 + 1./4.*0.098.^2.*bodymass.^0.70.*wingmass + 0.098.*bodymass.^0.35.*InertiaJ.*phi;
    Inertia = 2.* InertiaWingCenter + InertiaBody;
    %calculate maximum angular acceleration
    angular_acc = (L_max .*b_maxlift ./ 8) ./ Inertia; 
    roll_rate = 0.53 ./ sqrt(2.*0.53./angular_acc); 
    
    %calculate max sustained climb rate (a*v / g)
    %set the iterative lift vector
    L_start = repmat(L,1,length(v));
    cl_i = L_start ./ (0.5.*density.*area.*v.^2);
    cl_i(cl_i>clmax) = clmax(cl_i>clmax);
    TDi_flapping_i = c_torque.*(-cl_i.^2.*area.*density.*v.^2 ./ (2.*pi.*AR) + (1 - cl_i.^2./clmax.^2).*pi.^3 ./ 16 .* density .*AR.*area.*(sin(0.5.*theta).*f).^2 .* wl.^2);
    %calculate interative decrease in required lift
    lift = 1:10;
    thrust = 1:10;
    hoi(1) = L_start(400);
    thrust(1) = TDi_flapping_i(400);
    for n = 2:10    
        L_i = cos(asin(min(1,max(-1,(TDi_flapping_i-D)./(g.*m))))).*L_start;
        cl_i = L_i ./ (0.5.*density.*area.*v.^2);
        cl_i(cl_i>clmax) = clmax(cl_i>clmax);
        TDi_flapping_i = c_torque.*(-cl_i.^2.*area.*density.*v.^2 ./ (2.*pi.*AR) + (1 - cl_i.^2./clmax.^2).*pi.^3 ./ 16 .* density .*AR.*area.*(sin(0.5.*theta).*f).^2 .* wl.^2); 
        hoi(n) = L_i(400);
        thrust(n) = TDi_flapping_i(400);
    end
    %calculate climb rate:
    climb_rate = min(1,(TDi_flapping_i-D)./(g.*m)).*v;

    
    %____Calculate the lateral acceleration vs forward acceleration tradeoff
    %generate variables____________________________________________________
    Ls = (0.01:0.25:200)*m;
    speed = repmat(v,length(Ls),1);
    max_cl = repmat(clmax,length(Ls),1);
    Ls = repmat(Ls',1,length(v));
    %calculate tradeoff for gliding flight
    b_tradeoff = ((8.*Ls.^2.*bmax - 8.*Ls.^2.*bmin)./(pi.*Smax.*cfriction.*density.^2.*speed.^4)).^(1./3);
    b_tradeoff(b_tradeoff<bmin) = bmin + 0.000001;
    b_tradeoff(b_tradeoff>bmax) = bmax;
    cl_tradeoff = Ls ./ (0.5.*density.*Smax.*(b_tradeoff-bmin)./(bmax - bmin).*speed.^2);
    cl_tradeoff(cl_tradeoff>max_cl) = max_cl(cl_tradeoff>max_cl);
    b_tradeoff = Ls ./ (0.5.*density.*Smax.*cl_tradeoff./(bmax - bmin).*speed.^2) + bmin;
    b_tradeoff(b_tradeoff>bmax) = bmax;
    area2_tradeoff = Smax .* (b_tradeoff-bmin)./(bmax - bmin);
    AR2_tradeoff = b_tradeoff.^2./area2_tradeoff;
    D_tradeoff_glide =  cl_tradeoff.^2.*area2_tradeoff.*density.*speed.^2 ./ (2.*pi.*AR2_tradeoff) +  cfriction .* 0.5.*density.*area2_tradeoff .* speed.^2 + cbody .* 0.5.*density.*bodyArea .* speed.^2;
    L_tradeoff_glide = 0.5 .* cl_tradeoff .* density .* area2_tradeoff.*speed.^2;       
    %trade off for flapping
    cl_tradeoff_flap = Ls ./ (0.5.*density.*area.*speed.^2);
    cl_tradeoff_flap(cl_tradeoff_flap>max_cl) = max_cl(cl_tradeoff_flap>max_cl);
    induced = -cl_tradeoff_flap.^2.*area.*density.*speed.^2 ./ (2.*pi.*AR);
    TDi_tradeoff_flap = (1 - cl_tradeoff_flap.^2./max_cl.^2).*pi.^3 ./ 16 .* density .*AR.*area.*(sin(0.5.*theta).*f).^2 .* wl.^2 + induced;
    c_torque_tradeoff = min(1,cruisespeed./speed);
    TDi_tradeoff_flap = TDi_tradeoff_flap .*c_torque_tradeoff;
    D_tradeoff_flap = cbody .* 0.5.*density.*bodyArea .* speed.^2 + cfriction .* 0.5.*density.*area .* speed.^2;
    TD_tradeoff_flap = TDi_tradeoff_flap - D_tradeoff_flap;
    L_tradeoff_flap = cl_tradeoff_flap*0.5.*density.*area.*speed.^2;
    
    %dive of 100s
    dive_speed = 0;
    t = 0; 
    dt = 0.1;
    position_ = 0;
    position_max = 0;
    TDi_flapping_dive_ = pi.^3 ./ 16 .* density .*AR.*area.*(sin(0.5.*theta).*f).^2 .* wl.^2;
    
    for n = 1:1000
        c_torque_ = min(1,cruisespeed./dive_speed(n));
        TDi_flapping_dive_constr_ = TDi_flapping_dive_.*c_torque_;
        D_ = cbody .* 0.5.*density.*bodyArea .* dive_speed(n).^2 + cfriction .* 0.5.*density.*area .* dive_speed(n).^2;
        D2_ = cbody .* 0.5.*density.*bodyArea .* dive_speed(n).^2;
        ACCdive_(n) = max((TDi_flapping_dive_constr_ - D_),-D2_) ./ m + 9.81;
        position_(n+1) = position_(n) + dt*dive_speed(n);
        position_max(n+1) = position_max(n) + dt*v_max_dive;
        dive_speed(n+1) = dive_speed(n) + dt*ACCdive_(n);
        t(n+1) =  t(n) + dt;
    end
    
    %___________________generate output____________________________________
    output.airspeed = v;                                                          %speed from zero to 130 ms^-2
    output.max_forward_acc_level_flight = ACCmax;                                 %maximum forward acceleration given L = W
    output.max_lateral_acc =  L_max ./ m;                                         %maximum lateral acceleration achievable
    output.lateral_acc_given_cl_and_airspeed_flap = L_tradeoff_flap ./ m;         %lateral acceleration for a given cl and v when flapping
    output.forward_acc_given_cl_and_airspeed_flap = TD_tradeoff_flap ./ m;        %forward acceleration for a given cl and v when flapping
    output.lateral_acc_given_cl_and_airspeed_glide = L_tradeoff_glide ./ m;       %lateral acceleration for a given cl and v when gliding
    output.forward_acc_given_cl_and_airspeed_glide = D_tradeoff_glide ./ m;       %forward acceleration for a given cl and v when gliding
    output.top_level_speed = v_max_level;
    output.top_dive_speed = v_max_dive;
    output.minimum_power_speed = cruisespeed;
    output.cl_for_level_flight_flapping = cl;
    output.cl_for_level_flight_retracted_wings = cl_retracted;
    output.max_angular_acc = angular_acc;
    output.wingspan_for_level_glide = b;
    output.Inertia_given_retraction_level_flight = Inertia;
    output.roll_rate_in_90_degree_turn = roll_rate;
    output.friction_drag_retracted_wings = cfriction;
    output.max_forward_acc_dive = ACCdive;
    output.sustained_climb_rate = climb_rate;
     output.v_adjusted = v_adjusted;
     output.t = t;
     output.ACCdive_ = ACCdive_;
     output.position_ = position_;
     output.dive_speed = dive_speed;
     output.position_max = position_max;
     output.cl_tradeoff_flapping = cl_tradeoff_flap;
     output.sustained_load_factor = max((output.forward_acc_given_cl_and_airspeed_flap > 0) .* output.lateral_acc_given_cl_and_airspeed_flap) / g;
end





