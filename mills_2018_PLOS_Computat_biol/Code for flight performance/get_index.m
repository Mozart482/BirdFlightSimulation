function [ index ] = get_index(from, name )
    index = strfind(from, name);
    index = find(not(cellfun('isempty', index)));
end

