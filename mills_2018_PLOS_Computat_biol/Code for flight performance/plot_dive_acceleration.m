function [ p ] = plot_dive_acceleration(figureNumber, birds_morph, birds,style)
% plots the level acceleration versus airspeed for all birds
    lWidth = 3;
    mSize = 100;
    labelSize = 1;
    xSpan = 125;
    ySpan = 22;
    if style == 1; fig = figure(figureNumber); else
        fig = figure(1);
        subplot(3,2,figureNumber);
    end
    xSpan = 125;
    ySpan = 22;
    fig.RendererMode = 'manual';  % use a set with older versions of Matlab
    fig.Renderer = 'painters';
    hold on;
    num = 200;
    xlabel('airspeed (ms^{-1})', 'interpreter', 'Tex' );
    ylabel('vertical acceleration (ms^{-2})');
    ylim([0,22]);
    set(gca,'XTick',0:20:400);
    grid on;
    xlim([0,125]);
    plots = [];
    names = {};
    for n = 1:length(birds)
        the_bird = birds{n};
        the_bird_morph = birds_morph{n};
                if (ischar(the_bird_morph.plotColor)); pcolor = strcat('', the_bird_morph.plotColor); else; pcolor = the_bird_morph.plotColor; end
                plots(n) = plot(the_bird.airspeed,the_bird.max_forward_acc_dive,  'Color',pcolor, 'LineWidth', lWidth, 'MarkerSize', mSize);
                names{n} = [string(n){1} '. ' the_bird_morph.name{1}];
    end
    for n = 1:length(birds) 
        the_bird = birds{n};
        the_bird_morph = birds_morph{n};
        ySizeRectangle = ySpan ./ 35 .* labelSize;
        xSizeRectangle = xSpan ./ 35 .* labelSize;
        if (ischar(the_bird_morph.plotColor)); pcolor = strcat('', the_bird_morph.plotColor); else; pcolor = the_bird_morph.plotColor; end
        endpoint = find(abs(the_bird.airspeed - the_bird.top_dive_speed) == min(abs(the_bird.airspeed - the_bird.top_dive_speed)));
        rectangle('Position',[the_bird.airspeed(endpoint - 1500)-xSizeRectangle./2 the_bird.max_forward_acc_dive(endpoint - 1500)-ySizeRectangle./2 xSizeRectangle ySizeRectangle],'Curvature',1, 'FaceColor',[1 1 1],'EdgeColor', pcolor, 'LineWidth',lWidth/2);
        tx = text(the_bird.airspeed(endpoint - 1500),the_bird.max_forward_acc_dive(endpoint - 1500), string(n){1});
        tx.HorizontalAlignment = 'center';
        tx.FontSize = 12;
    end
    p = fig;
    title('(b)');
    leg = legend(plots,names);
end

