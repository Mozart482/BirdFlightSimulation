#ifndef LIBPARAM_HPP_INCLUDED
#define LIBPARAM_HPP_INCLUDED

#include "config.hpp"
#include <luabind\luabind.hpp>
#include "Params.hpp"


namespace libParam {

  using namespace Param;

	// FromLua ToLua
	template <typename T>
	T LIBSTARDISPLAY_API FromLua(const luabind::object& luaobj);

  template <typename T>
  luabind::object LIBSTARDISPLAY_API ToLua(lua_State* L, const T& cobj);

  template <> Roost       LIBSTARDISPLAY_API FromLua<Roost>(const luabind::object& luaobj);
  template <> RenderFlags LIBSTARDISPLAY_API FromLua<RenderFlags>(const luabind::object& luaobj);
  template <> Pursuit     LIBSTARDISPLAY_API FromLua<Pursuit>(const luabind::object& luaobj);
  template <> Params      LIBSTARDISPLAY_API FromLua<Params>(const luabind::object& luaobj);
  
  template <> luabind::object LIBSTARDISPLAY_API ToLua<Roost>(lua_State* L, const Roost& cobj);
  template <> luabind::object LIBSTARDISPLAY_API ToLua<RenderFlags>(lua_State* L, const RenderFlags& cobj);
}


#endif
