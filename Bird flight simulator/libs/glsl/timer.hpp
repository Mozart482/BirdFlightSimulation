//
// GLSL support library
// Hanno Hildenbrandt 2008
//

//! \file timer.hpp Support for \c EXT_timer_query

#ifndef GLSL_TIMER_HPP_INCLUDED
#define GLSL_TIMER_HPP_INCLUDED

#include "glsl.hpp"
#include "proxy.hpp"


namespace glsl {


  //! Thin Wrapper for \c EXT_timer_query.
  //!
  class timer : proxy<timer>
  {
  public:
    timer();
    ~timer();
    void swap(timer& other);  //!< Swap.

    bool isValid() const { return isValid_(); }
    void begin() const;      //!< Begin \c TIME_ELAPSED query.
    void end() const;      //!< End \c TIME_ELAPSED query.
    bool available() const;    //!< Returns \c true if the query result is available.

    //! Returns the elapsed time in seconds.
    //! Stalls until query result is available.
    double elapsed() const;

  private:
    friend class proxy<timer>;
    static void release(GLuint x) { glDeleteQueries(1, &x); }
  };

}


#endif
