#ifndef GLSL_GLSL_INCLUDED
#define GLSL_GLSL_INCLUDED

#include "gl_4_4.h"


#ifndef GLSL_LIB_BUILD
#  if defined (_MSC_VER)
#    pragma comment(lib, "opengl32.lib")
#    pragma comment(lib, "glsl.lib")
#  endif
#endif


namespace glsl 
{

  bool init();

}


#endif
