//
// GLSL support library
// Hanno Hildenbrandt 2008
//

//! \file attribute.hpp Thin wrapper for GLSL attributes.


#ifndef GLSL_ATTRIBUTE_HPP_INCLUDED
#define GLSL_ATTRIBUTE_HPP_INCLUDED

#include "glsl.hpp"
#include "proxy.hpp"


namespace glsl {

  class attribute
  {
  public:
    attribute(GLuint p, const char* name, GLint location = -1);
    attribute(GLuint p, GLuint index, GLint location = -1);

    GLuint index() const { return index_; }
    GLint size() const { return size_; }
    GLenum type() const { return type_; }
    GLint location() const { return location_; }
    const char* name() const { return name_.c_str(); }

  private:
    GLuint      index_;
    GLenum      type_;
    std::string name_;
    GLint       size_;
    GLint       location_;
  };

}  // namespace glsl


#endif



