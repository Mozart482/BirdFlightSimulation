#ifndef BMFFONT2_IO_HPP_INCLUDED
#define BMFFONT2_IO_HPP_INCLUDED

#include <stdint.h>
#include <string>
#include <vector>
#include <memory>


namespace glsl { namespace bmfont2 {


#pragma pack(push, 1)
  struct FileKerningPair
  {
    uint32_t first;
    uint32_t second;
    int16_t  amount;
  };


  struct FileChar
  {
    uint32_t id;
    uint16_t x;
    uint16_t y;
    uint16_t width;
    uint16_t height;
    int16_t  xoffset;
    int16_t  yoffset;
    int16_t  xadvance;
    uint8_t  page;
    uint8_t  chnl;
  };


  struct CommonBlk
  {
    int8_t   id;
    int32_t  size;
    uint16_t lineHeight;
    uint16_t base;
    uint16_t scaleW;
    uint16_t scaleH;
    uint16_t pages;
    uint8_t  bitField;
    uint8_t  alphaChnl;
    uint8_t  redChnl;
    uint8_t  greenChnl;
    uint8_t  blueChnl;
  };

#pragma pack(pop)


  // Loader of AngleCode's BMFont binary font files.
  // s. www.AngleCode.com
  //
  struct BMFontLoader
  {
#pragma pack(push, 1)
    struct InfoBlk
    {
      char     header[4];
      int8_t   id;
      int32_t  size;
      int16_t  fontSize;
      int8_t   bitField;
      uint8_t  charSet;
      uint16_t stretchH;
      uint8_t  aa;
      uint8_t  paddingUp;
      uint8_t  paddingRight;
      uint8_t  paddingDown;
      uint8_t  paddingLeft;
      uint8_t  spacingHoriz;
      uint8_t  spacingVerts;
      uint8_t  outline;
    };

#pragma pack(pop)


    BMFontLoader();
    void Parse(const char* BMFontDescr);

    InfoBlk*                 pInfoBlk;
    char*                    pFontName;
    CommonBlk*               pCommonBlk;
    std::vector<std::string> PagePath;
    size_t                   PageNameLen;
    int                      CharCount;
    FileChar*                pCharBlk;
    int                      KerningCount;
    FileKerningPair*         pKerningBlk;
    std::unique_ptr<char[]>  pFileBuffer;
  };

}}

#endif
