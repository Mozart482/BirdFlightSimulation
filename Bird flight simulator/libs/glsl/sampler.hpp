//
// GLSL support library
// Hanno Hildenbrandt 2008
//

//! \file sampler.hpp Thin wrapper for OpenGL sampler objects.

#ifndef GLSL_SAMPLER_HPP_INCLUDED
#define GLSL_SAMPLER_HPP_INCLUDED

#include "glsl.hpp"
#include "proxy.hpp"


namespace glsl {

  //! \class sampler
  //! Thin Wrapper for OpenGL sampler objects.
  class sampler : proxy<sampler>
  {
  public:
    //! Constructs 0-sampler
    sampler();

    //! Takes ownership over OpenGL sampler
    explicit sampler(GLuint samp);

    ~sampler();

    bool isValid() const { return isValid_(); }
    operator GLuint () const { return get(); }    //!< Cast to underlying OpenGL object.
    GLuint get() const;                //!< Returns the underlying OpenGL object.

    //! ALias to \c BindSampler(unit, this).
    void bind(GLuint unit) const;

    //! ALias to \c BindSampler(unit, 0).
    void unbind(GLuint unit) const;

    //! WRAP_S <- WRAP_T <- WRAP_R <- \c wrap.
    void set_wrap(GLint wrap) const;

    //! WRAP_S <- \c wrap_s, WRAP_T <- \c wrap_t, WRAP_R <- \c wrap_r.
    void set_wrap(GLint wrap_s, GLint wrap_t, GLint wrap_r) const;

    //! MIN_FILTER <- MAG_FILTER <- \c filter.
    void set_filter(GLint filter) const;

    //! MIN_FILTER <- min_filter, MAG_FILTER <- \c mag_filter.
    void set_filter(GLint min_filter, GLint mag_filter) const;

    //! Same effect as \c set_wrap(wrap); \c set_filter(filter).
    void set_wrap_filter(GLint wrap, GLint filter) const;

    void seti(GLenum pname, GLint param) const;          //!< Alias to \c glSamplerParameteri
    void setf(GLenum pname, GLfloat param) const;        //!< Alias to \c glSamplerParameterf
    void setfv(GLenum pname, const GLfloat* params) const;    //!< Alias to \c glSamplerParameterfv

    GLint geti(GLenum pname) const;                //!< Alias to \c glGetSamplerParameteriv
    GLfloat getf(GLenum pname) const;              //!< Alias to \c glGetSamplerParameterfv
    void getfv(GLenum pname, GLfloat* params) const;      //!< Alias to \c glGetSamplerParameterfv

      //! Swap
    void swap(sampler& other);

  private:
    friend class proxy<sampler>;
    static void release(GLuint x) { glDeleteSamplers(1, &x); }
  };

}

#endif
