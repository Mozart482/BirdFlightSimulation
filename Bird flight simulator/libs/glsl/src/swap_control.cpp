/*
 * vsync.cpp
 *
 *  Created on: Oct 29, 2009
 *      Author: hanno
 */

#include "swap_control.hpp"
#if defined(_WIN32) || defined(WIN32)
#include "wgl.h"
#else
#error "wgl not supported"
#endif


namespace glsl {

  void enable_vsync()
  {
    if (wgl_ext_EXT_swap_control) wglSwapIntervalEXT(1);
  }


  void disable_vsync()
  {
    if (wgl_ext_EXT_swap_control) wglSwapIntervalEXT(0);
  }

  
  void swap_control(int interval)
  {
    if (wgl_ext_EXT_swap_control) wglSwapIntervalEXT(interval);
  }

}
