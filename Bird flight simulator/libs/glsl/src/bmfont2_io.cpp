#include <fstream>
#include "bmfont2_io.hpp"


namespace glsl { namespace bmfont2 {


    BMFontLoader::BMFontLoader()
    : pInfoBlk(0), pFontName(0), pCommonBlk(0),
      PageNameLen(0),
      pCharBlk(0), pKerningBlk(0)
    {
    }

    void BMFontLoader::Parse(const char* BMFontDescr)
    {
      std::ifstream fs(BMFontDescr, std::ios_base::binary | std::ios_base::in);
      if (fs.fail())
      {
        throw "Open file failed";
      }

      // Read file into buffer
      std::streambuf* pbuf = fs.rdbuf();
      std::streamsize size = pbuf->pubseekoff(0, std::ios::end, std::ios::in);
      pbuf->pubseekpos(0, std::ios::in);
      pFileBuffer.reset(new char[size_t(size + 16)]);
      memset((void*)pFileBuffer.get(), '\0', size_t(size + 16));
      pbuf->sgetn(pFileBuffer.get(), size);

      // Sanity check
      if ((size < 5) || (0 != strncmp(pFileBuffer.get(), "BMF" "\3", 4)))
      {
        throw "Not a BMFont description file";
      }

      // Calculate reinterpreted pointer
      pInfoBlk = (InfoBlk*)(pFileBuffer.get());
      pFontName = (char*)(pInfoBlk)+sizeof(InfoBlk);
      pCommonBlk = (CommonBlk*)(pFontName + strlen(pFontName) + 1);
      int scaleH = pCommonBlk->scaleH;
      int scaleW = pCommonBlk->scaleW;
      if (scaleH > 256 || scaleW > 256)
      {
        throw "Texture dimension exceeds 256";
      }
      if (1 == (pCommonBlk->bitField & (1 << 7)))
      {
        throw "Packed format unsupported";
      }
      char* pPageName = (char*)(pCommonBlk)+sizeof(CommonBlk) + 5;
      PageNameLen = strlen(pPageName);
      ptrdiff_t prefixLen = std::distance(pPageName, strrchr(pPageName, '_'));
      std::string PagePathPrefix(BMFontDescr, strrchr(BMFontDescr, '.'));
      for (int page = 0; page < (int)pCommonBlk->pages; ++page, pPageName += PageNameLen + 1) {
        PagePath.emplace_back(PagePathPrefix + std::string(pPageName + prefixLen, pPageName + PageNameLen));
      }
      char* pBlock = pPageName;
      CharCount = *(int*)(pBlock + 1) / sizeof(FileChar);
      pCharBlk = (FileChar*)(pBlock + 5);
      pBlock = (char*)pCharBlk + CharCount * sizeof(FileChar);
      KerningCount = *(int*)(pBlock + 1) / sizeof(FileKerningPair);
      pKerningBlk = (FileKerningPair*)(pBlock + 5);
    }


}}
