//
// GLSL support library
// Hanno Hildenbrandt 2008
//

#include "renderbuffer.hpp"
#include <cassert>


namespace glsl {


  renderbuffer::renderbuffer()
  {
    GLuint rbo; glGenRenderbuffers(1, &rbo);
    reset_(rbo);
  }

  renderbuffer::renderbuffer(GLuint rbo)
  :  proxy<renderbuffer>(rbo)
  {
  }

  renderbuffer::renderbuffer(GLenum internalFormat, GLsizei width, GLsizei height)
  {
    GLuint rbo; glGenRenderbuffers(1, &rbo);
    reset_(rbo);
    bind();
    storage(internalFormat, width, height);
    unbind();
  }

  renderbuffer::~renderbuffer()
  {
  }

  void renderbuffer::bind() const
  {
    glBindRenderbuffer(GL_RENDERBUFFER, get());
  }

  void renderbuffer::unbind() const
  {
    glBindRenderbuffer(GL_RENDERBUFFER, 0);
  }

  void renderbuffer::storage(GLenum internalFormat, GLsizei width, GLsizei height) const
  {
    glRenderbufferStorage(GL_RENDERBUFFER, internalFormat, width, height);
  }

  void renderbuffer::multisampleStorage(GLsizei samples, GLenum internalFormat, GLsizei width, GLsizei height) const
  {
    glRenderbufferStorageMultisample(GL_RENDERBUFFER, samples, internalFormat, width, height);
  }

  void renderbuffer::swap(renderbuffer& a)
  {
    swap_(a);
  }


}  // namespace glsl
