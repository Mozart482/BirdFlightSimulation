//
// GLSL support library
// Hanno Hildenbrandt 2008
//

#include <cassert>
#include <algorithm>
#include <memory>
#include "shader.hpp"


namespace glsl {


  shader::shader()
  {
  }


  shader::shader(const char* shaderSource, GLenum shaderType)
  {
    GLuint sh = glCreateShader(shaderType);
    if (0 != sh)
    {
      glShaderSource(sh, 1, &shaderSource, 0);
      glCompileShader(sh);
      reset_(sh);
    }
  }


  shader::~shader(void)
  {
  }


  GLint shader::status(GLenum pname) const
  {
    GLint status = 0;
    if (get() != 0) glGetShaderiv(get(), pname, &status);
    return status;
  }


  GLenum shader::type() const
  {
    GLint t = GL_NONE;
    if (get() != 0) glGetShaderiv(get(), GL_SHADER_TYPE, &t);
    return (GLenum)t;
  }

  std::string shader::log_info() const
  {
    GLcharARB log[1024] = {0};
    if (get() != 0) glGetShaderInfoLog(get(), 1024, 0, log);
    return std::string(log);
  }


  std::string shader::source() const
  {
    if (!isValid()) return std::string();
    GLint sLength; glGetShaderiv(get(), GL_SHADER_SOURCE_LENGTH, &sLength);
    std::unique_ptr<GLchar[]> buf(new GLchar[sLength]);
    if (sLength == 0) return std::string();
    glGetShaderSource(get(), sLength, &sLength, buf.get());
    return std::string(buf.get());
  }


  void shader::swap(shader& s)
  {
    swap_(s);
  }


}  // namespace glsl
