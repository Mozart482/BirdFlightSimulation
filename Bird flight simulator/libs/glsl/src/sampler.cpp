#include "sampler.hpp"


namespace glsl {

  sampler::sampler()
  {
  }

  sampler::sampler(GLuint samp)
  :   proxy<sampler>(samp)
  {
  }

  sampler::~sampler()
  {
  }

  GLuint sampler::get() const
  {
    GLuint h = get_();
    if (0 == h) { glGenSamplers(1, &h); this->reset_(h); }
    return h;
  }

  void sampler::set_wrap(GLint wrap) const
  {
    set_wrap(wrap, wrap, wrap);
  }

  void sampler::set_wrap(GLint wrap_s, GLint wrap_t, GLint wrap_r) const
  {
    glSamplerParameteri(get(), GL_TEXTURE_WRAP_S, wrap_s);
    glSamplerParameteri(get(), GL_TEXTURE_WRAP_T, wrap_t);
    glSamplerParameteri(get(), GL_TEXTURE_WRAP_R, wrap_r);
  }

  void sampler::set_filter(GLint filter) const
  {
    set_filter(filter, filter);
  }

  void sampler::set_filter(GLint min_filter, GLint mag_filter) const
  {
    glSamplerParameteri(get(), GL_TEXTURE_MAG_FILTER, mag_filter);
    glSamplerParameteri(get(), GL_TEXTURE_MIN_FILTER, min_filter);
  }

  void sampler::set_wrap_filter(GLint wrap, GLint filter) const
  {
    set_wrap(wrap, wrap, wrap);
    set_filter(filter, filter);
  }

  void sampler::seti(GLenum pname, GLint param) const
  {
    glSamplerParameteri(get(), pname, param);
  }

  void sampler::setf(GLenum pname, GLfloat param) const
  {
    glSamplerParameterf(get(), pname, param);
  }

  void sampler::setfv(GLenum pname, const GLfloat* params) const
  {
    glSamplerParameterfv(get(), pname, params);
  }

  GLint sampler::geti(GLenum pname) const
  {
    GLint param[4]; glGetSamplerParameteriv(get(), pname, param);
    return param[0];
  }

  GLfloat sampler::getf(GLenum pname) const
  {
    GLfloat param[4]; glGetSamplerParameterfv(get(), pname, param);
    return param[0];
  }

  void sampler::getfv(GLenum pname, GLfloat* params) const
  {
    glGetSamplerParameterfv(get(), pname, params);
  }

  void sampler::bind(GLuint unit) const
  {
    glBindSampler(unit, get());
  }

  void sampler::unbind(GLuint unit) const
  {
    glBindSampler(unit, 0);
  }

  void sampler::swap(sampler& other)
  {
    swap_(other);
  }


}
