/*
 * binary_find.hpp
 *
 *  Created on: Aug 4, 2010
 *      Author: hanno
 */

#ifndef GLSL_CACHE_HPP_INCLUDED
#define GLSL_CACHE_HPP_INCLUDED

#include <cassert>
#include <algorithm>
#include <string.h>
#include <type_traits>
#include <glsl.hpp>
#include "exception.hpp"


namespace glsl { namespace detail {


  template<typename T>
  struct cached_elem_cmp
  {
    bool operator()(T const& a, const char* b) const
    {
      return 0 > strcmp(a->name(), b);
    }
    bool operator()(const char* a, T const& b) const
    {
      return 0 > strcmp(a, b->name());
    }
    bool operator()(T const& a, T const& b) const
    {
      return 0 > strcmp(a->name(), b->name());
    }
  };


  template<typename C>
  typename C::value_type cache(GLuint p, C& c, const char* name)
  {
    typedef typename C::value_type value_type;
    typedef typename C::iterator iterator;
    typedef typename std::remove_pointer<value_type>::type pointee_type;

    cached_elem_cmp<value_type> cmp;
    iterator first(c.begin());
    iterator last(c.end());
    iterator it = std::lower_bound(first, last, name, cmp);
    if ((it != last) && (0 == strcmp((*it)->name(), name))) {
      return *it;  // already in cache
    }
    pointee_type* pt = new pointee_type(p, name);
    if (pt->index() == -1)
    {
      throw glsl::exception(std::string("GLSL resource '") + name + "' inaccessible");
    }
    c.insert(it, pt);    // insert sorted
    return pt;
  }


}}

#endif
