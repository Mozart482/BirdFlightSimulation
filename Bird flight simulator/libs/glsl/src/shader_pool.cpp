#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <unordered_set>
#include "shader_pool.hpp"


namespace glsl {  namespace {


   const char delim[] = "\n[]{};:";


    enum block_type
    {
      VOID_BLOCK,
      VERTEX_SHADER_BLOCK,
      GEOMETRY_SHADER_BLOCK,
      FRAGMENT_SHADER_BLOCK,
      MAX_BLOCK_TYPE,
      VERTEX_SHADER_SOURCE,
      GEOMETRY_SHADER_SOURCE,
      FRAGMENT_SHADER_SOURCE,
      PROGRAM_NAME
    };


    const char* block_type_name[MAX_BLOCK_TYPE] = {
      "void",
      "vertex",
      "geometry",
      "fragment"
    };


    struct block
    {
      const block_type  type;
      const std::string name;
      const std::string body;
      const int         declLine; 

      block(block_type Type, const std::string& Name, const std::string& Body = "", int DeclLine = 0)
        : type(Type), name(Name), body(Body), declLine(DeclLine)
      {}
    };


    // make associative container happy
    struct block_hash : public std::unary_function<block, std::size_t> 
    {
      std::size_t operator()(block const& a) const
      {
        std::hash<std::string> hstr;
        return hstr(a.name) + a.type;
      }
    };


    bool operator==(block const& a, block const& b)
    {
      return ((a.type == b.type) && (a.name == b.name));
    }
  
    class parser
    {
    public:
      typedef std::unordered_set<block, block_hash> block_set;

      parser(std::istream& is);
      const block_set& blocks() const { return blocks_; }
      const char* findSource(block_type type, const std::string& name) const
      {
        auto it = blocks_.find(block(type, name));
        return (it == blocks_.end()) ? 0 : it->body.c_str();
      }

    private:
      block_type testShaderType(std::string& typeName) const;
      bool isdelim(const char ch);
      std::string next_token(std::istream& is);
      void parse_named_block(std::istream& is, block_type type);
      void parse_list(std::istream& is);
      void parse_shader_block(std::istream& is);
      void parse_program_block(std::istream& is);

      int       line_;
      int       cline_;
      char      delimiter_;
      block_set blocks_;
    };


    bool parser::isdelim(const char ch)
    {
      delimiter_ = ch;
      if (isspace(ch)) return true;
      for (int i=0; i<sizeof(delim)/sizeof(char); ++i)
      {
        if (ch == delim[i]) { return true; };
      }
      return false;
    }


    std::string parser::next_token(std::istream& is)
    {
      std::string tok;
      delimiter_ = ' ';
      while (tok.empty() && isspace(delimiter_))
      {
        for (;;)
        {
          char ch = 0;
          is >> ch;
          if (!is) return "eof";
          else if (ch == '\n') ++line_;
          else if (ch == '\r') ;          // ignore
          else if (ch == '/')
          {
            is >> ch;
            if (ch == '/')
            {
              is.ignore(1024, std::char_traits<char>::to_int_type('\n'));
              ++line_;
              break;
            }
            else
            {
              is.putback(ch);
            }
          }
          else if (isdelim(ch)) break;
          else
          {
            cline_ = line_;
            tok += ch;
          }
        }
      }
      return tok;
    }


    void parser::parse_named_block(std::istream& is, block_type type)
    {
      std::string name = next_token(is);
      if (name.empty()) throw "name expected";
      if (delimiter_ != '{') throw "{ expected";
      cline_ = line_;
      int copen = 1;
      std::string body;
      while (!!is)
      {
        char ch = 0;
        is >> ch;
        if (ch == '{') ++copen;
        if (ch == '}') --copen;
        if (ch == '\n') ++line_;
        if (0 == copen) break;
        body += ch;
      }
      if (!is) throw "unbalanced '{'";
      std::string stop = next_token(is);
      if (!(stop.empty() && delimiter_ == ';')) throw "';' expected";
      blocks_.insert(block(type, name, body, cline_));
    }


    void parser::parse_shader_block(std::istream& is)
    {
      if (delimiter_ != '[') 
      {
        std::string start = next_token(is);
        if (!(start.empty() && delimiter_ == '[')) throw "'[' expected";
      }
      std::string type = next_token(is);
      block_type bt = testShaderType(type);
      if (delimiter_ != ']') 
      {
        std::string stop = next_token(is);
        if (!(stop.empty() && delimiter_ == ']')) throw "']' expected";
      }
      parse_named_block(is, bt);
    }


    void parser::parse_program_block(std::istream& is)
    {
      std::string name = next_token(is);
      int declLine = line_;
      if (name.empty()) throw "name expected";
      if (delimiter_ != '{') throw "{ expected";
      std::string version;
      {
        auto it = blocks_.find(block(VOID_BLOCK, "GLSL_VERSION"));
        if (it != blocks_.end()) version = it->body;
      }
      while (!!is)
      {
        std::string type = next_token(is);
        if (delimiter_ == '}') break;
        block_type bt = testShaderType(type);
        if (delimiter_ != ':') throw "':' excpected";
        std::ostringstream ss;
        ss << version;
        while (delimiter_ != ';')
        {
          std::string blockName = next_token(is);
          if (blockName == "*") blockName = name;
          auto it = blocks_.find(block((block_type)bt, blockName));
          if (it == blocks_.end())
          {
            it = blocks_.find(block(VOID_BLOCK, blockName));
            if (it == blocks_.end()) throw "undefined block name";
          }
          ss << "#line " << it->declLine+1 << it->body;
        }
        blocks_.insert(block(block_type(bt + MAX_BLOCK_TYPE), name, ss.str(), declLine));
      }
      if (!is) throw "unbalanced '{'";
      std::string stop = next_token(is);
      if (!(stop.empty() && delimiter_ == ';')) throw "';' expected";
      blocks_.insert(block(PROGRAM_NAME, name, "", declLine));
    }


    parser::parser(std::istream& is)
      : line_(1), cline_(1), delimiter_('x')
    {
      try
      {
        is >> std::noskipws;
        while (!!is)
        {
          std::string tok = next_token(is);
          if (tok == "void")
            parse_named_block(is, VOID_BLOCK);
          else if (tok == "shader")
            parse_shader_block(is);
          else if (tok == "program")
            parse_program_block(is);
        }
        if (blocks_.find(block(VOID_BLOCK, "GLSL_VERSION")) == blocks_.end())
        {
          throw "please provide GLSL_VERSION block";
        }
      }
      catch (const char* msg)
      {
        std::ostringstream ss;
        ss << "glsl::shader_pool: Parsing error in line " << cline_ << ": " << msg << '.';
        glDebugMessageInsert(
          GL_DEBUG_SOURCE_THIRD_PARTY,
          GL_DEBUG_TYPE_ERROR, 0xCECE,
          GL_DEBUG_SEVERITY_HIGH, -1,
          ss.str().c_str());
      }
    }


    block_type parser::testShaderType(std::string& typeName) const
    {
      int bt = 1;
      for (; bt<MAX_BLOCK_TYPE; ++bt)
      {
        if (typeName == block_type_name[bt]) break;
      }
      if (bt == MAX_BLOCK_TYPE) throw "invalid shader type";
      return (block_type)bt;
    }

  }


  void shader_pool::parse(std::istream& is)
  {
    bool ret = true;
    parser parser(is);
    for (const block& b : parser.blocks())
    {
      if (b.type == PROGRAM_NAME)
      {
        glsl::program prog(
          parser.findSource(VERTEX_SHADER_SOURCE, b.name),
          parser.findSource(FRAGMENT_SHADER_SOURCE, b.name),
          parser.findSource(GEOMETRY_SHADER_SOURCE, b.name)
        );
        prog_map_[b.name] = prog;
      }
    }
  }


  glsl::program* shader_pool::operator ()(const char* progName)
  {
    auto p = prog_map_.find(progName);
    if (p == prog_map_.end()) {
      std::string msg = std::string("Program \"") + progName + "\" not found in shader pool.";
      glDebugMessageInsert(
        GL_DEBUG_SOURCE_THIRD_PARTY,
        GL_DEBUG_TYPE_ERROR, 0xCECE,
        GL_DEBUG_SEVERITY_HIGH, -1,
        msg.c_str());
      return nullptr;
    }
    return &(*p).second;
  }


  bool shader_pool::add_named_program(const char* name, const glsl::program& prog)
  {
    return prog_map_.insert(prog_map::value_type(name, prog)).second;
  }
  

  glsl::buffer* shader_pool::get_named_buffer(const char* name)
  {
    auto p = buffer_map_.find(name);
    if (p == buffer_map_.end()) {
      std::string msg = std::string("Named buffer \"") + name + "\" not found in shader pool.";
      glDebugMessageInsert(
        GL_DEBUG_SOURCE_THIRD_PARTY,
        GL_DEBUG_TYPE_ERROR, 0xCECE,
        GL_DEBUG_SEVERITY_HIGH, -1,
        msg.c_str());
      return nullptr;
    }
    return &(*p).second;
  }


  bool shader_pool::add_named_buffer(const char* name)
  {
    return buffer_map_.insert(buffer_map::value_type(name, glsl::buffer())).second;
  }

  
  void shader_pool::swap(shader_pool& other)
  {
    prog_map_.swap(other.prog_map_);
    buffer_map_.swap(other.buffer_map_);
  }

}

