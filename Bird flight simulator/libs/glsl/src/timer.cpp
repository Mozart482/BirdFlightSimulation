/*
 * timer.cpp
 *
 *  Created on: Aug 24, 2010
 *      Author: hanno
 */

#include <cassert>
#include "timer.hpp"


namespace glsl {

  timer::timer()
  {
  }

  timer::~timer()
  {
  }

  void timer::begin() const
  {
    GLuint query = get_();
    if (0 == query) { glGenQueries(1, &query); this->reset_(query); }
    glBeginQuery(GL_TIME_ELAPSED, query);
  }

  void timer::end() const
  {
    glEndQuery(GL_TIME_ELAPSED);
  }

  bool timer::available() const
  {
    GLint available = false;
    glGetQueryObjectiv(get_(), GL_QUERY_RESULT_AVAILABLE, &available);
    return (GL_TRUE == available);
  }

  double timer::elapsed() const
  {
    GLuint query = get_();
    if (!query) return 0.0;
    while (!this->available()) {}
    GLuint64EXT timeElapsed;
    glGetQueryObjectui64v(query, GL_QUERY_RESULT, &timeElapsed);
    return double(timeElapsed) * 0.000000001;
  }

}
