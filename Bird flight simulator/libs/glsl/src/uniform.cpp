//
// GLSL support library
// Hanno Hildenbrandt 2008
//

#include "uniform.hpp"


namespace glsl {


  uniform::uniform()
  : index_(GL_INVALID_INDEX), type_(GL_NONE), size_(0), loc_(-1) 
  {
  }


  uniform::uniform(GLuint p, GLint index)
  : index_(GL_INVALID_INDEX), type_(GL_NONE), size_(0), loc_(-1)
  {
    GLsizei len = 0;
    GLchar uname[256] = { 0 };
    glGetActiveUniform(p, index, 255, &len, &size_, &type_, uname);
    if (len) {
      index_ = index;
      name_ = uname;
      loc_ = glGetUniformLocation(p, uname);
    }
  }


  uniform::uniform(GLuint p, const char* name)
  : index_(GL_INVALID_INDEX), type_(GL_NONE), size_(0), loc_(-1)
  {
    GLchar uname[256] = { 0 };
    glGetUniformIndices(p, 1, &name, &index_);
    if (index_ != GLuint(-1)) 
    {
      glGetActiveUniform(p, index_, 255, 0, &size_, &type_, uname);
      loc_ = glGetUniformLocation(p, uname);
      name_ = uname;
    } 
  }


}  // namespace glsl





