//
// GLSL support library
// Hanno Hildenbrandt 2010
//

#include "gpu_memory_info.hpp"


#ifndef GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX
#  define GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX          0x9047
#  define GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX    0x9048
#  define GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX  0x9049
#  define GPU_MEMORY_INFO_EVICTION_COUNT_NVX            0x904A
#  define GPU_MEMORY_INFO_EVICTED_MEMORY_NVX            0x904B
#  define GLSL_UNDEF_GPU_MEMORY_INFO
#endif


namespace glsl {

  GLint gpu_memory_info_nvx(GLenum X)
  {
    GLint res = -1;
    glGetIntegerv(X, &res);
    return res;
  }

  //! Dedicated video memory, total size (in kb) of the GPU memory.
  //! Returns -1 if NVX_gpu_memory_info is not supported.
  GLint dedicated_vidmem_nvx()
  {
    return gpu_memory_info_nvx(GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX);
  }

  //! total available memory, total size (in Kb) of the memory available for allocations.
  //! Returns -1 if NVX_gpu_memory_info is not supported.
  GLint total_available_memory_nvx()
  {
    return gpu_memory_info_nvx(GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX);
  }

  //! current available dedicated video memory (in kb), currently unused GPU memory.
  //! Returns -1 if NVX_gpu_memory_info is not supported.
  GLint current_available_vidmem_nvx()
  {
    return gpu_memory_info_nvx( GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX);
  }

  //! count of total evictions seen by system.
  //! Returns -1 if NVX_gpu_memory_info is not supported.
  GLint eviction_count_nvx()
  {
    return gpu_memory_info_nvx(GPU_MEMORY_INFO_EVICTION_COUNT_NVX);
  }

  //! size of total video memory evicted (in kb).
  //! Returns -1 if NVX_gpu_memory_info is not supported.
  GLint evicted_memory_nvx()
  {
    return gpu_memory_info_nvx(GPU_MEMORY_INFO_EVICTED_MEMORY_NVX);
  }

}

#ifdef GLSL_UNDEF_GPU_MEMORY_INFO
#  undef GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX
#  undef GPU_MEMORY_INFO_TOTAL_AVAILABLE_MEMORY_NVX
#  undef GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX
#  undef GPU_MEMORY_INFO_EVICTION_COUNT_NVX
#  undef GPU_MEMORY_INFO_EVICTED_MEMORY_NVX
#  undef GLSL_UNDEF_GPU_MEMORY_INFO
#endif


