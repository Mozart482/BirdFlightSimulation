//
// GLSL support library
// Hanno Hildenbrandt 2008
//

#include <vector>
#include <algorithm>
#include "program.hpp"
#include "cache.hpp"


namespace glsl {


  namespace {

    GLint getProgramiv(GLuint p, GLenum pname)
    {
      GLint param;
      glGetProgramiv(p, pname, &param);
      return param;
    }

  }


  struct sh_prog_state
  {
    explicit sh_prog_state(GLuint p): prog(p) {}
    ~sh_prog_state() 
    {
      for (auto p : attributeCache) { delete p; }
      for (auto p : uniformCache) { delete p; }
      for (auto p : uniformBlockCache) { delete p; }
      for (auto p : transformFeedbackVaryingCache) { delete p; }
      glDeleteProgram(prog); 
    }

    void clearCache()
    {
      attributeCache.clear();
      uniformCache.clear();
      uniformBlockCache.clear();
      transformFeedbackVaryingCache.clear();
    }

    GLuint prog;
    shader vertexShader;
    shader fragmentShader;
    shader geometryShader;
    std::vector<glsl::attribute*>     attributeCache;
    std::vector<glsl::uniform*>       uniformCache;
    std::vector<glsl::uniform_block*> uniformBlockCache;
    std::vector<glsl::transform_feedback_varying*>  transformFeedbackVaryingCache;
  };


  program::program()
  {
  }

  program::program(const char* vertexSource,
                   const char* fragmentSource,
                   const char* geometrySource)
  {
    if (vertexSource) {
      attach( shader(vertexSource, GL_VERTEX_SHADER) );
    }
    if (fragmentSource) {
      attach( shader(fragmentSource, GL_FRAGMENT_SHADER) );
    }
    if (geometrySource) {
      attach( shader(geometrySource, GL_GEOMETRY_SHADER) );
    }
  }


  program::program(shader const& vertexShader,
                   shader const& fragmentShader,
                   shader const& geometryShader)
  {
    attach(vertexShader);
    attach(fragmentShader);
    attach(geometryShader);
  }


  program::~program(void)
  {
  }


  GLuint program::get() const
  {
    return (state_) ? state_->prog : 0;
  }


  bool program::attach(shader const& sh)
  {
    if (0 == get()) state_.reset( new sh_prog_state(glCreateProgram()) );
    switch (sh.type())
    {
      case GL_VERTEX_SHADER: state_->vertexShader = sh; break;
      case GL_FRAGMENT_SHADER: state_->fragmentShader = sh; break;
      case GL_GEOMETRY_SHADER: state_->geometryShader = sh; break;
      default: return false;
    }
    glAttachShader(get(), sh);
    return true;
  }


  bool program::detach(GLenum type)
  {
    if (0 == get()) return true;
    switch (type)
    {
      case GL_VERTEX_SHADER: glDetachShader(get(), state_->vertexShader); state_->vertexShader = shader(); break;
      case GL_FRAGMENT_SHADER: glDetachShader(get(), state_->fragmentShader); state_->fragmentShader = shader(); break;
      case GL_GEOMETRY_SHADER: glDetachShader(get(), state_->geometryShader); state_->geometryShader = shader(); break;
      default: return false;
    }
    return true;
  }


  bool program::link()
  {
    if (0 == get()) return false;
    glLinkProgram(get());
    GLint status = getProgramiv(get(), GL_LINK_STATUS);
    state_->clearCache();
    return (GL_TRUE == status);
  }


  glsl::program* program::use() const
  {
    glUseProgram(get());
    return const_cast<glsl::program*>(this);
  }


  bool program::validate() const
  {
    glValidateProgram(get());
    return GL_TRUE == status(GL_VALIDATE_STATUS);
  }


  GLint program::status(GLenum pname) const
  {
    return getProgramiv(get(), pname);
  }


  std::string program::log_info() const
  {
    GLcharARB log[1024] = {0};
    GLsizei length;
    glGetProgramInfoLog(get(), 1024, &length, log);
    std::string tmp("Program info log:\n");
    tmp += log;
    glsl::shader sh = get_shader(GL_VERTEX_SHADER);
    if (sh.isValid()) { tmp += "\nVertex shader info log:\n"; tmp += sh.log_info(); }
    sh = get_shader(GL_GEOMETRY_SHADER);
    if (sh.isValid()) { tmp += "\nGeometry shader info log:\n"; tmp += sh.log_info(); }
    sh = get_shader(GL_FRAGMENT_SHADER);
    if (sh.isValid()) { tmp += "\nFragment shader info log:\n"; tmp += sh.log_info(); }
    return tmp;
  }


  shader program::get_shader(GLenum type) const
  {
    switch (type)
    {
      case GL_VERTEX_SHADER: return state_->vertexShader; break;
      case GL_FRAGMENT_SHADER: return state_->fragmentShader; break;
      case GL_GEOMETRY_SHADER: return state_->geometryShader; break;
    }
    return shader();
  }


  void program::swap(program& other)
  {
    state_.swap(other.state_);
  }


  GLint program::geometry_input() const
  {
    return (0 == get_shader(GL_GEOMETRY_SHADER)) ? GL_NONE : getProgramiv(get(), GL_GEOMETRY_INPUT_TYPE);
  }


  GLint program::geometry_output() const
  {
    return (0 == get_shader(GL_GEOMETRY_SHADER)) ? GL_NONE : getProgramiv(get(), GL_GEOMETRY_OUTPUT_TYPE);
  }


  GLint program::geometry_vertices() const
  {
    return (0 == get_shader(GL_GEOMETRY_SHADER)) ? GL_NONE : getProgramiv(get(), GL_GEOMETRY_VERTICES_OUT);
  }


  GLuint program::active_uniforms() const
  {
    GLint n;
    glGetProgramiv(get(), GL_ACTIVE_UNIFORMS, &n);
    return (GLuint)n;
  }


  glsl::uniform* program::uniform(const char* uname)
  {
    return detail::cache(get(), state_->uniformCache, uname);
  }


  GLuint program::active_attributes() const
  {
    GLint n;
    glGetProgramiv(get(), GL_ACTIVE_ATTRIBUTES, &n);
    return (GLuint)n;

  }


  glsl::attribute* program::attribute(const char* aname, GLint location)
  {
    if (-1 != location) glBindAttribLocation(get(), location, aname);
    return detail::cache(get(), state_->attributeCache, aname);
  }


  GLuint program::active_uniform_blocks() const
  {
    return (GLuint)getProgramiv(get(), GL_ACTIVE_UNIFORM_BLOCKS);
  }


  glsl::uniform_block* program::uniform_block(const char* bname)
  {
    return detail::cache(get(), state_->uniformBlockCache, bname);
  }


  GLuint program::active_varyings() const
  {
    GLint n;
    glGetProgramiv(get(), GL_TRANSFORM_FEEDBACK_VARYINGS, &n);
    return (GLuint)n;
  }


  glsl::transform_feedback_varying* program::varying(const char* vname)
  {
    return detail::cache(get(), state_->transformFeedbackVaryingCache, vname);
  }


  void program::feedback_varyings(std::vector<std::string> const& varyings, bool interleaved)
  {
    if (!varyings.empty())
    {
      GLenum mode = interleaved ? GL_INTERLEAVED_ATTRIBS : GL_SEPARATE_ATTRIBS;
      size_t n = varyings.size();
      const GLchar** var = new const GLchar*[n];
      for (size_t i=0; i<n; ++i) var[i] = varyings[i].c_str();
      glTransformFeedbackVaryings(get(), (GLsizei)n, var, mode);
      delete[] var;
    }
  }


}  // namespace glsl
