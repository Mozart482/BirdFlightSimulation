//
// GLSL support library
// Hanno Hildenbrandt 2008
//

#include "framebuffer.hpp"


namespace glsl  {


  void framebuffer::blit(int srcX0, int srcY0, int srcX1, int srcY1,
               int dstX0, int dstY0, int dstX1, int dstY1,
               GLbitfield mask, GLenum filter)
  {
    glBlitFramebuffer(srcX0, srcY0, srcX1, srcY1, dstX0, dstY0, dstX1, dstY1, mask, filter);
  };

  framebuffer::framebuffer()
  :  target_(GL_NONE)
  {
  }

  framebuffer::framebuffer(GLuint fbo)
  :  proxy<framebuffer>(fbo), target_(GL_NONE)
  {
  }

  framebuffer::~framebuffer()
  {
  }

  GLenum framebuffer::status() const
  {
    return glCheckFramebufferStatus(target_);
  }

  void framebuffer::bind(GLenum target) 
  {
    target_ = target;
    if (!get()) {
      GLuint x; glGenFramebuffers(1, &x);
      reset_(x);
    }
    glBindFramebuffer(target_, get());
  }

  void framebuffer::unbind()
  {
    if (GL_NONE != target_)
    {
      glBindFramebuffer(target_, 0);
      target_ = GL_NONE;
    }
  }

  void framebuffer::attachRenderbuffer(GLenum attachment, GLuint rbo) const
  {
    glFramebufferRenderbuffer(target_, attachment, GL_RENDERBUFFER, rbo);
  }

  void framebuffer::attachTexture1D(GLenum attachment, GLuint texture, GLint level) const
  {
    glFramebufferTexture1D(target_, attachment, GL_TEXTURE_1D, texture, level);
  }

  void framebuffer::attachTexture2D(GLenum attachment, GLuint texture, GLint level) const
  {
    glFramebufferTexture2D(target_, attachment, GL_TEXTURE_2D, texture, level);
  }

  void framebuffer::attachTexture3D(GLenum attachment, GLuint texture, GLint zoffset, GLint level) const
  {
    glFramebufferTexture3D(target_, attachment, GL_TEXTURE_3D, texture, level, zoffset);
  }

  void framebuffer::attachTextureLayer(GLenum attachment, GLuint texture, GLint level, GLint layer) const
  {
    glFramebufferTextureLayer(target_, attachment, texture, level, layer);
  }

  void framebuffer::attachTextureRectangle(GLenum attachment, GLuint texture, GLint level) const
  {
    glFramebufferTexture2D(target_, attachment, GL_TEXTURE_RECTANGLE, texture, level);
  }

  void framebuffer::swap(glsl::framebuffer &other)
  {
    swap_(other);
    GLenum etmp = target_; target_ = other.target_; other.target_ = etmp;
  }


}  // namespace glsl



