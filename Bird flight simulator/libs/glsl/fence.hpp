//
// GLSL support library
// Hanno Hildenbrandt 2008
//

//! \file fence.hpp Support for ARB_sync

#ifndef GLSL_FENCE_HPP_INCLUDED
#define GLSL_FENCE_HPP_INCLUDED

#include "glsl.hpp"
#include "proxy.hpp"


namespace glsl {


  //! Thin Wrapper for ARB_sync.
  //!
  class fence : proxy<fence, GLsync>
  {
  public:
    //! Constructs a fence object.
    fence();
    ~fence();

    bool isValid() const { return isValid_(); }
    operator GLsync () const { return get(); }    //!< Cast to underlying OpenGL object.
    GLsync get() const;                //!< Returns the underlying OpenGL object.

    //! Wait for the fence to be signaled or until the \c timeout period (ns) expires .
    //! A call to \c client_wait blocks GL.
    //! \returns one of \c GL_TIMEOUT_EXPIRED, GL_CONDITION_SATISFIED, GL_ALREADY_SIGNALED or GL_WAIT_ERROR
    GLenum client_wait(GLuint64 timeout = GL_TIMEOUT_IGNORED);

    //! Request the GL server to wait for the fence to be signaled.
    //! Returns immediately but causes the GL server to block.
    void wait();

    void swap(fence& other);    //!< Swap.

  private:
    friend class proxy<fence, GLsync>;
    static void release(GLsync x) { glDeleteSync(x); }
  };

}


#endif
