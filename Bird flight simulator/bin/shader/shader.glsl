// BFS Shader 
//
// Hanno Hildenbrandt & Robin Mills 2013-2018
//


void GLSL_VERSION
{
  #version 440 core
};


// Transformation uniform block
void Matrices
{
  layout (std140) uniform Matrices
  {
    mat4 ModelViewProjection;
    mat4 ModelView;
    mat4 Projection;
    mat4 Ortho;
  };
};


void rotationMatrix
{
  mat3 rotationMatrix(vec3 axis, float angle)
  {
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;
    
    return mat3(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c);
  };
};


void fogFactor
{
  float fogFactor(float z)
  {
    float ff = 1.0 - smoothstep(1250.0, 2000.0, z); 
    return clamp(ff, 0.0, 1.0);
  };
};


shader [vertex] Text
{
  layout (location = 0) in vec2 TexCoord;
  layout (location = 1) in vec2 Vertex;
  layout (location = 2) in vec4 Color;

  out vec2 vTexCoord;
  out vec2 vVertex;
  out vec4 vColor;

  void main()
  {
    vColor = Color;
    vTexCoord = TexCoord;
    vVertex = Vertex;
  }
};

shader [geometry] Text
{
  layout(lines) in;
  layout(triangle_strip, max_vertices=4) out;

  in vec2 vTexCoord[2];
  in vec2 vVertex[2];
  in vec4 vColor[2];

  smooth out vec2 gTexCoord;
  flat out vec4 gColor;

  void main()
  {
    vec2 t0 = vTexCoord[0];
    vec2 t1 = vTexCoord[1];
    vec2 v0 = vVertex[0];
    vec2 v1 = vVertex[1];

    gColor = vColor[1];
    gTexCoord = vec2(t0.x, t1.y);
    gl_Position = Ortho * vec4(v0.x, v1.y, 0, 1);
    EmitVertex();

    gTexCoord = vec2(t0.x, t0.y);
    gl_Position = Ortho * vec4(v0.x, v0.y, 0, 1);
    EmitVertex();

    gTexCoord = vec2(t1.x, t1.y);
    gl_Position = Ortho * vec4(v1.x, v1.y, 0, 1);
    EmitVertex();

    gTexCoord = vec2(t1.x, t0.y);
    gl_Position = Ortho * vec4(v1.x, v0.y, 0, 1);
    EmitVertex();

    EndPrimitive();
  }
};

shader [fragment] Text
{
  #extension GL_ARB_texture_rectangle : enable
  
  uniform sampler2DRect FontTexture;

  smooth in vec2 gTexCoord;
  flat in vec4 gColor;

  layout (location = 0) out vec4 FragColor;

  void main()
  {
    vec4 color = gColor;
    color.a = texture2DRect(FontTexture, gTexCoord).r;
    FragColor = color;
  }
};

program Text
{
  vertex: *;
  geometry: Matrices *;
  fragment: *;
};


shader[vertex] NoLit
{
  layout (location = 0) in vec4 Vertex;
  layout (location = 1) in vec4 Color;
  layout (location = 2) in float ColorTexCoord;

  smooth out vec4 vColor;
  smooth out float vColorTexCoord;

  void main(void) 
  {            
    vColor = Color;
    vColorTexCoord = ColorTexCoord;
    gl_Position = ModelViewProjection * Vertex;
  }

};

shader[fragment] NoLit
{
  uniform sampler1D ColorTex; 

  smooth in vec4 vColor;
  smooth in float vColorTexCoord;
  
  out vec4 FragColor;

  void main(void) 
  {
    if (vColorTexCoord <= -90.0) discard;
    FragColor = (vColorTexCoord <= 90.0) ? texture(ColorTex, vColorTexCoord) : vColor;
  }
};

program NoLit
{
    vertex: Matrices *;
    fragment: *;
};


shader[vertex] NoLit2D
{
  layout (location = 0) in vec4 Vertex;
  layout (location = 1) in vec4 Color;
  layout (location = 2) in float ColorTexCoord;

  smooth out vec4 vColor;
  smooth out float vColorTexCoord;

  void main(void) 
  {            
    vColor = Color;
    vColorTexCoord = ColorTexCoord;
    gl_Position = Ortho * Vertex;
  }

};

program NoLit2D
{
    vertex: Matrices *;
    fragment: NoLit;
};


shader[vertex] SkyBox
{
  vec3[12*3] vertices = vec3[12*3]( 
    vec3(1,  1,  1),   vec3(1, -1, -1),   vec3(1, -1,  1),
    vec3(1, -1, -1),   vec3(1,  1,  1),   vec3(1,  1, -1),
    vec3(-1,  1, -1),  vec3(-1, -1,  1),  vec3(-1, -1, -1),
    vec3(-1, -1,  1),  vec3(-1,  1, -1),  vec3(-1,  1,  1),
    vec3(1, -1, -1),   vec3(-1,  1, -1),  vec3(-1, -1, -1),
    vec3(-1,  1, -1),  vec3(1, -1, -1),   vec3(1,  1, -1),
    vec3(1,  1,  1),   vec3(-1, -1,  1),  vec3(-1,  1,  1),
    vec3(-1, -1,  1),  vec3(1,  1,  1),   vec3( 1, -1,  1),
    vec3(1,  1,  1),   vec3(-1,  1, -1),  vec3(1,  1, -1),
    vec3(-1,  1, -1),  vec3(1,  1,  1),   vec3(-1,  1,  1),
    vec3(-1, -1,  1),  vec3(1, -1, -1),   vec3(-1, -1, -1),
    vec3(1, -1, -1),   vec3(-1, -1,  1),  vec3(1, -1,  1)
  );

  out vec3 vViewDir;

  void main(void)
  {
    gl_Position = Projection * vec4(mat3(ModelView) * vertices[gl_VertexID], 1.0);
    vViewDir = vertices[gl_VertexID];
  }
};


shader[fragment] SkyBox
{
  uniform vec4 colorFact;
  uniform samplerCube CubeMap;

  in vec3 vViewDir;
  out vec4 FragColor;

  void main(void) 
  { 
    FragColor = colorFact * texture( CubeMap, vViewDir );
    gl_FragDepth = 1.0;
  }
};


program SkyBox
{
    vertex: Matrices *;
    fragment: *;
};


shader[vertex] StaticInstancing
{
  struct static_attrib_t
  {
    vec4 forward_scale;
    vec4 up;
    vec4 position;
  };

  layout (location = 0) in vec2 TexCoord;
  layout (location = 1) in vec4 Normal;
  layout (location = 2) in vec4 Vertex;
  layout (binding = 0, std140) buffer static_block
  {
    static_attrib_t static_attribs[];
  };

  const vec4 Eye = vec4(0.0, 0.0, 1.0, 0.0);
  uniform float diffuse = 1.0;  
  uniform float ambient = 0.0;
  uniform vec2  alphaMask = vec2(0,1);
  
  smooth out vec2  vTexCoord;
  smooth out float vShade;

  void main(void)
  {
    static_attrib_t sa = static_attribs[gl_InstanceID];
    // construct body matrix
    mat4 M = mat4( vec4(sa.forward_scale.xyz, 0.0), 
                   vec4(sa.up.xyz, 0.0),
                   vec4(cross(sa.forward_scale.xyz, sa.up.xyz), 0.0), 
                   sa.position);
    vTexCoord = TexCoord;                             // Object texture

    // Normal in view space (remains normalized)
    vec4 normal = ModelView * M * vec4(Normal.xyz, 0);

    // Add an simple headlight (two-sided)
    float ds = abs(dot(normal, Eye));
    vShade = ambient + diffuse * ds;
    
    // Vertex in local space
    vec4 position = Vertex;
    position.xyz *= sa.forward_scale.w;
    position = M * position;
    gl_Position = ModelViewProjection * position;
  }
};


shader[fragment] StaticInstancing
{
  uniform sampler2D Texture;

  smooth in vec2  vTexCoord;
  smooth in float vShade;

  out vec4 FragColor;
  
  void main( void )
  {
    FragColor = vShade * texture( Texture, vTexCoord );
  }

};

program StaticInstancing
{
  vertex: Matrices *;
  fragment: *;
};


shader[vertex] WingInstancing
{
  struct static_attrib_t
  {
    vec4 forward_scale;
    vec4 up;
    vec4 position;
  };

  struct wing_attrib_t
  {
    float scale;
    float whatever;
	float beatCycle;
	float RS;
  };

  layout (location = 0) in vec2 TexCoord;
  layout (location = 1) in vec4 Normal;
  layout (location = 2) in vec4 Vertex;
  layout (binding = 0, std140) buffer static_block
  {
    static_attrib_t static_attribs[];
  };
  layout (binding = 1, std140) buffer wing_block
  {
    wing_attrib_t wing_attribs[];
  };

  const vec4 Eye = vec4(0.0, 0.0, 1.0, 0.0);
  uniform float diffuse = 1.0;
  uniform float ambient = 0.0;
  uniform vec2  alphaMask = vec2(0, 1);

  smooth out vec2  vTexCoord;
  smooth out float vShade;

  void main(void)
  {
    static_attrib_t sa = static_attribs[gl_InstanceID];
    wing_attrib_t wa = wing_attribs[gl_InstanceID];
    // construct body matrix
    mat4 M = mat4(vec4(sa.forward_scale.xyz, 0.0),
                  vec4(sa.up.xyz, 0.0),
                  vec4(cross(sa.forward_scale.xyz, sa.up.xyz), 0.0),
                  sa.position);
    vTexCoord = TexCoord;                             // Object texture
    // Normal in view space (remains normalized)
    vec4 normal = ModelView * M * vec4(Normal.xyz, 0);

	float amplitude = 0.78f;
	float t = 0.75f;
	float bone_connect = 1.0f/5.0f;
	float psweep = 0.0;
	
    float length_sec2 = sqrt(1 + psweep*psweep);
    float hoek = asin(wa.RS / length_sec2);
    float sweep_ = cos(hoek) * length_sec2;
    sweep_ = 0.5*(sweep_ - psweep) + psweep;
    float x_add = bone_connect * psweep / 2 - 0.02;
    float y_add = pow(bone_connect,1.2) * (sweep_ - psweep)*0.5;
    
    float sweep = 1.25*abs(abs(Vertex.z) - bone_connect)*float(abs(Vertex.z)>bone_connect)*(sweep_ - psweep) - x_add;
	sweep += pow(abs(abs(Vertex.z) - bone_connect),1.2)*psweep;
    
    float sweep_y = pow(abs(abs(Vertex.z) - bone_connect),1.2)*(0.5*float(abs(Vertex.z)>bone_connect) +0.5)*(sweep_ - psweep) - y_add;



	float wingSec = Vertex.z - max(min(Vertex.z, 0.06), -0.06);
	float yr = wa.RS*abs(wingSec) * sin(amplitude*cos(wa.beatCycle)) - sweep_y;
	float zr = Vertex.z - wingSec + wa.RS*wingSec * cos(amplitude*cos(wa.beatCycle));
	float xr = Vertex.x - sweep;

    // Add an simple headlight (two-sided)
    float ds = abs(dot(normal, Eye));
    vShade = ambient + diffuse * ds;

    // Vertex in local space
    vec4 position = Vertex;
	position.y = yr;
	position.z = zr;
	position.x = xr;
    //position.xyz *= sa.forward_scale.w;
	position.xyz *=   wa.scale;
    position = M * position;
    gl_Position = ModelViewProjection * position;
  }
};


program WingInstancing
{
  vertex: Matrices *;
  fragment: StaticInstancing;
};


shader[vertex] Ribbon
{
  layout (location = 0) in vec4 side_T;       // vSide vector & simulation time
  layout (location = 1) in vec4 pos_U;        // position & color texture coord
  layout (location = 2) in float RS; 
  layout (location = 3) in float beatCycle;
  layout (location = 4) in vec4 up_s;       // vSide vector & simulation time

  out float vColorTexCoord;    // color texture
  out float vTime;             // Time
  out vec3 vPosition;
  out vec3 vSide;              // Side vector
  out vec3 vUp;
  out float vBeatCycle;
  out float vRS;

  void main(void)
  {
    vTime = side_T.w;
    vPosition = pos_U.xyz; // + mod(beatCycle / (2.0f*3.14f),1);
    vColorTexCoord = pos_U.w;
    vSide = side_T.xyz  * up_s.w;
	vUp = up_s.xyz;
	vBeatCycle = beatCycle;
	vRS = RS;
  }
};


shader[geometry] Ribbon
{
  layout(lines_adjacency) in;
  layout(triangle_strip, max_vertices = 6) out;

  const vec4 eye = vec4(0.0, 0.0, 1.0, 0.0);
  const float diffuse = 0.5;
  const float ambient = 1.0 - diffuse;

  uniform sampler1D colorTex;
  uniform float halfWidth;

  in float vTime[4];              // time
  in float vColorTexCoord[4];     // color texture
  in vec3 vPosition[4];           // position
  in vec3 vSide[4];               // vSide vector
  in vec3 vUp[4];
  in float vBeatCycle[4];
  in float vRS[4];

  smooth out vec4 gColor;
  smooth out float gSimTime;
  smooth out float gShade;

  float shade(vec3 normal)
  {
    float ds = abs(dot(ModelView * vec4(normal, 0.0), eye));
    return diffuse * ds + ambient;
  }

  void Emit(vec3 v)
  {
    gl_Position = ModelViewProjection * vec4(v, 1.0);
    EmitVertex();
  }

  float rand(float n){return fract(sin(n) * 43758.5453123);}


  void main(void)
  {
    float skip = vTime[0] * vTime[1] * vTime[2] * vTime[3];
    if (skip == 0) return;

    vec3 tangent01 = normalize(vPosition[1] - vPosition[0]);
    vec3 tangent21 = normalize(vPosition[2] - vPosition[1]);
    vec3 tangent23 = normalize(vPosition[3] - vPosition[2]);

    vec3 up0 = cross(vSide[0], tangent01);
    vec3 up1 = cross(vSide[1], tangent21);
    vec3 up2 = cross(vSide[2], tangent23);

    float shade0 = shade(up0);
    float shade1 = shade(up1);
    float shade2 = shade(up2);

    vec4 color0 = texture(colorTex, vColorTexCoord[0]);
    vec4 color1 = texture(colorTex, vColorTexCoord[1]);
    vec4 color2 = texture(colorTex, vColorTexCoord[2]);

	float amplitude = 0.78f;
	float t = 0.75f;
	float bone_connect = 1.0f/5.0f;
	float psweep = 0.44;
	
    float length_sec2 = sqrt(1 + psweep*psweep);
    float hoek = asin(vRS[1] / length_sec2);
    float sweep_ = cos(hoek) * length_sec2;
    sweep_ = 0.5*(sweep_ - psweep) + psweep;
    float x_add = bone_connect * psweep / 2 - 0.02;
    float y_add = pow(bone_connect,1.2) * (sweep_ - psweep)*0.5;
    
    float sweep = 1.25*abs(abs(halfWidth) - bone_connect)*float(abs(halfWidth)>bone_connect)*(sweep_ - psweep) - x_add;
	sweep += pow(abs(abs(halfWidth) - bone_connect),1.2)*psweep;
    
    float sweep_y = pow(abs(abs(halfWidth) - bone_connect),1.2)*(0.5*float(abs(halfWidth)>bone_connect) +0.5)*(sweep_ - psweep) - y_add;

	vec3 forward = cross(vUp[1], vSide[1]);

	float wingSec = vPosition[1].z - max(min(vPosition[1].z, 0.06), -0.06);
	//Emit(vPosition[1]);

	
	gColor = mix(color0, color1, 0.5);
    gShade = mix(shade0, shade1, 0.5);
    gSimTime = vTime[1];
    
	
    Emit(vPosition[1] - halfWidth * vRS[1] *(vSide[1]*cos(amplitude*cos(vBeatCycle[1])) - vUp[1]*sin(amplitude*cos(vBeatCycle[1]))));
    Emit(vPosition[2] - halfWidth * vRS[2] *  (vSide[2]*cos(amplitude*cos(vBeatCycle[2])) - vUp[2]*sin(amplitude*cos(vBeatCycle[2]))));
	Emit(vPosition[1]);
	Emit(vPosition[2]);
	Emit(vPosition[1] + halfWidth * vRS[1] *(vSide[1]*cos(amplitude*cos(vBeatCycle[1])) + vUp[1]*sin(amplitude*cos(vBeatCycle[1]))));
    Emit(vPosition[2] + halfWidth * vRS[2] *  (vSide[2]*cos(amplitude*cos(vBeatCycle[2])) + vUp[2]*sin(amplitude*cos(vBeatCycle[2]))));

	Emit(vPosition[1] - halfWidth * vRS[1] *(vSide[1]*cos(amplitude*cos(vBeatCycle[1])) - vUp[1]*sin(amplitude*cos(vBeatCycle[1]))));

gSimTime = vTime[2];

    
    Emit(vPosition[1]);
	Emit(vPosition[2]);
    
    
	Emit(vPosition[1] + halfWidth * vRS[1] *  (vSide[2]*cos(amplitude*cos(vBeatCycle[1])) + vUp[1]*sin(amplitude*cos(vBeatCycle[1]))));
	Emit(vPosition[1]);
	Emit(vPosition[2]);


	//Emit(vPosition[1] + halfWidth * vRS[1] *(vSide[1]*cos(amplitude*cos(vBeatCycle[1])) + vUp[1]*sin(amplitude*cos(vBeatCycle[1]))));

	//Emit(vPosition[1] + halfWidth * vRS[1] *  (vSide[1]*cos(amplitude*cos(vBeatCycle[1])) + vUp[1]*sin(amplitude*cos(vBeatCycle[1]))));

    gColor = mix(color1, color2, 0.5);
	gColor.a = 0.4;
    gShade = mix(shade1, shade2, 0.5);
    
	//Emit(vPosition[2]);

   
 
	//Emit(vPosition[2] + halfWidth * vRS[2] *  (vSide[2]*cos(amplitude*cos(vBeatCycle[2])) + vUp[2]*sin(amplitude*cos(vBeatCycle[2]))));
   
	
	
  }
};


shader[fragment] Ribbon
{
  uniform float oneOverTickInterval;
  uniform float oneMinusTickWidth;

  smooth in vec4 gColor;
  smooth in float gSimTime;
  smooth in float gShade;

  out vec4 FragColor;

  void main(void)
  {
    float tickIntensity = step(oneMinusTickWidth, fract(oneOverTickInterval * gSimTime));
    FragColor = gShade * clamp(gColor + tickIntensity, 0.0, 1.0) + 0.25 * tickIntensity;
    FragColor.a = 0.4;
  }
};


program Ribbon
{
  vertex: *;
  geometry: Matrices *;
  fragment: *;
};


shader [vertex] Disk
{
  layout (location = 0) in float Radius;
  out float vRadius;

  void main()
  {
    vRadius = Radius;
  }
};


shader [fragment] Stripe
{
  uniform vec4 color = vec4(1.0, 0.5, 0.5, 1.0);
  
  smooth in float s;
  out vec4 FragColor;
 
  void main()
  {
    FragColor = color;
    FragColor.a *= fogFactor(gl_FragCoord.z / gl_FragCoord.w);
  }
};


shader [geometry] DiskRadii
{
  layout(points) in;
  layout(line_strip, max_vertices=108) out;

  in float vRadius[1];
  smooth out float s;
  smooth out float viewDist;

  void main()
  {
    for (int i=0; i<360; i+=10)
    {
      float angle = radians(float(i));
      vec3 d = vec3(cos(angle), 0.0, sin(angle));

      s = 0.0;
      viewDist = (ModelView * vec4(0.0, 0.0, 0.0, 1.0)).z;
      gl_Position = ModelViewProjection * vec4(0.0, 0.0, 0.0, 1.0);
      EmitVertex();
      
      s = vRadius[0];
      viewDist = (ModelView * vec4(d * vRadius[0], 1.0)).z;
      gl_Position = ModelViewProjection * vec4(d * vRadius[0], 1.0);
      EmitVertex();
      
      EndPrimitive();
    }
  }
};


shader [vertex] Widget
{
  layout (location = 0) in vec4 vAttribs;     // tex, vert

  out vec2 vTexCoord;
  out vec2 vVertex;

  void main()
  {
    vTexCoord = vAttribs.xy;
    vVertex = vAttribs.zw;
  }
};

shader [geometry] Widget
{
  layout(lines) in;
  layout(triangle_strip, max_vertices=4) out;

  in vec2 vTexCoord[2];
  in vec2 vVertex[2];

  smooth out vec2 gTexCoord;

  void main()
  {
    vec2 t0 = vTexCoord[0];
    vec2 t1 = vTexCoord[1];
    vec2 v0 = vVertex[0];
    vec2 v1 = vVertex[1];

    gTexCoord = vec2(t0.x, t1.y);
    gl_Position = Ortho * vec4(v0.x, v1.y, 0, 1);
    EmitVertex();

    gTexCoord = vec2(t0.x, t0.y);
    gl_Position = Ortho * vec4(v0.x, v0.y, 0, 1);
    EmitVertex();

    gTexCoord = vec2(t1.x, t1.y);
    gl_Position = Ortho * vec4(v1.x, v1.y, 0, 1);
    EmitVertex();

    gTexCoord = vec2(t1.x, t0.y);
    gl_Position = Ortho * vec4(v1.x, v0.y, 0, 1);
    EmitVertex();

    EndPrimitive();
  }
};

shader [fragment] Widget
{
  uniform sampler2D WidgetTexture;
  uniform vec4 color;

  smooth in vec2 gTexCoord;
  out vec4 FragColor;

  void main()
  {
    float alpha = texture(WidgetTexture, gTexCoord).r;
    FragColor = vec4(color.xyz, alpha * color.a);
  }
};

program Widget
{
  vertex: *;
  geometry: Matrices *;
  fragment: *;
};


shader [vertex] Widget3D
{
  layout (location = 0) in vec4 Vertex;
  layout (location = 1) in vec2 TexCoord;
  
  uniform mat4 H = mat4(1.0);

  out vec2 vTexCoord;

  void main()
  {
    vTexCoord = TexCoord;
    gl_Position = ModelViewProjection * H * Vertex;
  }
};

shader [fragment] Widget3D
{
  uniform sampler2D Texture;
  uniform vec4 color;

  smooth in vec2 vTexCoord;
  out vec4 FragColor;

  void main()
  {
    float alpha = texture(Texture, vTexCoord).r;
    FragColor = vec4(color.xyz, alpha * color.a);
  }
};

program Widget3D
{
  vertex: Matrices *;
  fragment: *;
};


