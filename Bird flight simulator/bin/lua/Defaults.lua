print("  sourcing Defaults.lua")


local Default = {

  Roost = {
    numPrey = 200,
    numPredators = 0,
    Radius = 100.0,
    minRadius = 50.0,
    maxRadius = 500.0,
  },


  Ruler = {
    Tick = 1,        -- [m]
    MinFlockSize = 1
  },


    evolution = {
		type ="PN",
		pred_fitness_criterion = "pred_stat.minDist",
		prey_fitness_criterion = nil,
		fileName = "default.txt",
		durationGeneration = 25.0,   -- length in seconds in which one duration hunts
		startGen = 0,				-- When no randomization at start is desired, set it to >0 (such as when loading an old one)
		load = false,
		loadFolder = "",
		Trajectories = {amount = 0, dt = 0.1,},
		externalPrey = false,
		stationaryPrey = false,
		artificialPrey = false,
		artificialPredator = false,
		externalPreyFile = "pos_lure_flight1.txt",
		oneOnOnePredPrey = false,
		title = "Default Title",
		description = "default description",
		terminationGeneration = 100000000,

		evolving_parameters = {
			--{name = "predBird_params.InitialPosition.y", type = "normal", a = 0, b = 0.1, scale = true, initial = {min = 0, max = 600}, },
		    --{name = "predBird_params.InitialPosition.x", type = "normal", a = 0, b = 0.1, scale = true, initial = {min = 0, max = 600}, },
			--{name = "pred_params.N", type = "normal", a = 0, b = 0.1, scale = true, initial = {min = 0, max = 100}, },
		
		},

		to_be_saved = {
		   {"pred_stat.minDist"},
		   {"pred_stat.velocityMinDist"},
		   {"pred_stat.seqTime"},
		   {"predator.id"},
		   {"predBird_params.generation"},
		   {"pred_stat.InterceptionState", template = "TrajectoryTable()"},
		},

		random_variables = {
		  {name = "preyBird_params.reactionTime",  type = "normal", a = 0.07, b = 0.001},
		  {name = "predBird_params.InitialPosition.y",  type = "uniform", a = -100, b = 2000},
		  {name = "predBird_params.InitialPosition.x",  type = "uniform", a = 0, b = 800},
		  {name = "pred_params.N",  type = "uniform", a = 1, b = 20},
          {name = "preyBird_params.InitialHeading",  type = "vec_in_sphere", template = "{x=0,y=0,z=0}"},
		},
  },

  Birds = {
     csv_convertor = "..\\..\\lua\\XlsToCsv.vbs",
     csv_file_species_xlsm = "lua\\bird_properties.xlsm",
     csv_file_species = "../../lua/bird_properties.csv",
	 csv_file_prey_predator_settings = "../../lua/prey_predator_settings.csv",
  },

    DataStorage = {
     folder_lua = "..\\data\\",
     folder = "../data/",
  },

  Trail = {
    Length = 5,           -- length in seconds (read-once)
    Width = 0.25,         -- [m]
    TickInterval = 0.5,   -- tick each trailTickInterval (s)
    TickWidth = 0.05,     -- fraction of trailTickInterval
    Skip = 2              -- skiped integration steps
  },


  RenderFlags = {
    show_trails = false,
    show_annotation = true,
    show_header = true,
    show_fps = true,
    slowMotion = false,
    helpMsg = "",
	turnOffGraphics = false,
  },

}


return Default
