print("  sourcing config.lua")
require "config_functions"
--____________________________________________________________________settings___________________________________________________________________
--experiment or free flying?
doExperiments = 1

-- Overwrite default initial parameters if required
gParam.Roost.numPrey = 1
gParam.Roost.numPredators = 1
gParam.Roost.Radius = 500.0
gParam.Roost.minRadius = 150.0
gParam.Roost.maxRadius = 10000.0
gParam.evolution.type = "PN"
gParam.evolution.durationGeneration = 1
gParam.evolution.Trajectories.amount = 0
gParam.RenderFlags.turnOffGraphics = false


--after changing the settings as desired, run the experiments lua file:
dofile(Simulation.WorkingPath .. "experiments.lua")

