#ifndef BFS_RANDOM_HPP_INCLUDED
#define BFS_RANDOM_HPP_INCLUDED

#include <random>
#include <Eigen/Dense>

typedef std::mt19937 rnd_eng_type;


// Seeding
void rnd_seed(unsigned long seed);

// Returns thread local random number engine
rnd_eng_type& rnd_eng();


struct normal_random_variable
{
	normal_random_variable(Eigen::MatrixXd const& covar)
		: normal_random_variable(Eigen::VectorXd::Zero(covar.rows()), covar)
	{}

	normal_random_variable(Eigen::VectorXd const& mean, Eigen::MatrixXd const& covar)
		: mean(mean)
	{
		Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eigenSolver(covar);
		transform = eigenSolver.eigenvectors() * eigenSolver.eigenvalues().cwiseSqrt().asDiagonal();
	}

	Eigen::VectorXd mean;
	Eigen::MatrixXd transform;

	Eigen::VectorXd operator()() const
	{
		static std::mt19937 gen{ std::random_device{}() };
		static std::normal_distribution<> dist;

		return mean + transform * Eigen::VectorXd{ mean.size() }.unaryExpr([&](auto x) { return dist(gen); });
	}
};




#endif  // OMP_RANDOM_HPP_INCLUDED
