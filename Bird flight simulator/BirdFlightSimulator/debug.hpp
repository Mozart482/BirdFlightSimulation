#ifndef BFS_DEBUG_HPP_INCLUDED
#define BFS_DEBUG_HPP_INCLUDED

#include <exception>
#include <glsl/glsl.hpp>


namespace debug
{

  extern int DebugLogStackLevel;


  void CppStackDump();


  void __stdcall GLDebugLog(GLenum source, GLenum type, GLuint id, GLenum severity,
                            GLsizei length, const GLchar* message, const GLvoid*);


  void __stdcall GLDebugLogOnce(GLenum source, GLenum type, GLuint id, GLenum severity,
                                GLsizei length, const GLchar* message, const GLvoid*);

}

#endif
