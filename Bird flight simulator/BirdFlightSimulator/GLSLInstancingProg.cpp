#include <glsl/shader_pool.hpp>
#include "mapped_buffer.hpp"
#include "GLSLInstancingProg.hpp"
#include "GLSLState.hpp"
#include "GLSLModel.hpp"
#include "Params.hpp"
#include "Bird.hpp"
#include "Globals.hpp"


GLSLStaticInstancingProg::GLSLStaticInstancingProg(unsigned ModelId, unsigned MaxN)
  : instances_(0), model_(CreateModel(ModelId)), pprog_(nullptr), MaxN_(MaxN)
{
  pprog_= GGl.use_program(PARAMS.ModelSet[ModelId].shader.c_str());
  pprog_->uniform_block("Matrices")->binding(0);
}


GLSLStaticInstancingProg::~GLSLStaticInstancingProg() 
{
}


void GLSLStaticInstancingProg::Flush()
{
  ssbo_.bind(GL_SHADER_STORAGE_BUFFER);
  ssbo_.flush_range(0, sizeof(static_attrib_t) * instances_);
  ssbo_.unmap();
}


void GLSLStaticInstancingProg::Render()
{
  model_->Bind();
  pprog_->use();
  pprog_->uniform("Texture")->set1i(0);
  pprog_->uniform("ambient")->set1f(model_->material().amb.x);
  pprog_->uniform("diffuse")->set1f(1.0f - model_->material().amb.x);
  ssbo_.bind(GL_SHADER_STORAGE_BUFFER);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, ssbo_.vbo());
  if (ssbo_.is_flushed()) 
  {
    glDrawElementsInstanced(GL_TRIANGLES, model_->nIndices(), GL_UNSIGNED_INT, (GLvoid*)(0), GLsizei(instances_));
  }
  model_->Unbind();
}


GLSLBodyWingInstancingProg::GLSLBodyWingInstancingProg(unsigned BodyModelId, unsigned WingModelId, unsigned MaxN)
  : body_(BodyModelId, MaxN), instances_(0), model_(CreateModel(WingModelId)), pprog_(nullptr), MaxN_(MaxN)
{
  pprog_ = GGl.use_program(PARAMS.ModelSet[WingModelId].shader.c_str());
  pprog_->uniform_block("Matrices")->binding(0);
}


GLSLBodyWingInstancingProg::~GLSLBodyWingInstancingProg()
{
}


void GLSLBodyWingInstancingProg::Flush()
{
  body_.Flush();
  ssbo_.bind(GL_SHADER_STORAGE_BUFFER);
  ssbo_.flush_range(0, sizeof(wing_attrib_t) * instances_);
  ssbo_.unmap();
}


void GLSLBodyWingInstancingProg::Render()
{
  body_.Render();
  model_->Bind();
  pprog_->use();
  pprog_->uniform("Texture")->set1i(0);
  pprog_->uniform("ambient")->set1f(model_->material().amb.x);
  pprog_->uniform("diffuse")->set1f(1.0f - model_->material().amb.x);
  // body ssbo is still bound to base 0
  ssbo_.bind(GL_SHADER_STORAGE_BUFFER);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, ssbo_.vbo());
  if (ssbo_.is_flushed())
  {
    glDrawElementsInstanced(GL_TRIANGLES, model_->nIndices(), GL_UNSIGNED_INT, (GLvoid*)(0), GLsizei(instances_));
  }
  model_->Unbind();
}


