#include <algorithm>
#include <numeric>
#include <glmutils/istream.hpp>
#include "Globals.hpp"
#include "GLSLState.hpp"
#include "GLSLModel.hpp"
#include "GLSLiotexture.hpp"
#include "Params.hpp"
#include "ac3d.hpp"


std::unique_ptr<GLSLModel> CreateModel(unsigned ModelId)
{
  const Param::ModelDef& modelDef = PARAMS.ModelSet[ModelId];
  return std::unique_ptr<GLSLModel>(new GLSLModel(modelDef));
}


GLSLModel::GLSLModel(const Param::ModelDef& modelDef) 
: nIndices_(0),
  nVertices_(0),
  modelScale_(modelDef.Scale)
	
{
  std::cout << "\n model scale: " << modelDef.Scale;
  ac3d_model model = ImportAC3D(filesystem::path(GGl.MediaPath()) / "ac3d" / "birds", modelDef.acFile)[modelDef.id - 1];
  nVertices_ = (GLsizei)model.vertices.size();
  nIndices_ = (GLsizei)model.indices.size();
  texture_ = LoadTextureRGBA(model.texFile, true, false);
  texture_.set_wrap_filter(GL_CLAMP_TO_EDGE, GL_LINEAR_MIPMAP_LINEAR, GL_LINEAR);
  twoSided_ = model.twoSided;
  material_ = model.material;
  vao_.bind();
  vbuffer_.bind(GL_ARRAY_BUFFER);
  ibuffer_.bind(GL_ELEMENT_ARRAY_BUFFER);
  vbuffer_.data(sizeof(T2F_N3F_V3F) * nVertices_, 0, GL_STATIC_DRAW);
  ibuffer_.data(sizeof(GLint) * nIndices_, 0, GL_STATIC_DRAW);
  vbuffer_.sub_data(0, sizeof(T2F_N3F_V3F) * nVertices_, (GLvoid*)(model.vertices.data()));
  ibuffer_.sub_data(0, sizeof(GLint) * nIndices_, (GLvoid*)(model.indices.data()));
  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);
  glEnableVertexAttribArray(2);
  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(T2F_N3F_V3F), 0);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(T2F_N3F_V3F), (void*)(2*sizeof(float)));
  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(T2F_N3F_V3F), (void*)(5*sizeof(float)));
}


GLSLModel::~GLSLModel()
{
}


void GLSLModel::Bind()
{
  if (twoSided_) glDisable(GL_CULL_FACE);    // hints to bad model design
  texture_.bind(0);
  vao_.bind();
}


void GLSLModel::Unbind()
{
  if (twoSided_) glEnable(GL_CULL_FACE);    // back to default
}


