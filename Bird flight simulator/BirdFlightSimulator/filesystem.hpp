#ifndef FILESYSTEM_HPP_INCLUDED
#define FILESYSTEM_HPP_INCLUDED


#include <filesystem>
namespace filesystem = std::experimental::filesystem;

/*
// Adding missing operator/ in tr2 proposal

inline filesystem::path operator/(const filesystem::path& lhs, const std::string& rhs)
{
  return filesystem::path(lhs) /= rhs;
}

inline filesystem::path operator/(const filesystem::path& lhs, const char* rhs)
{
  return filesystem::path(lhs) /= rhs;
}

inline filesystem::path operator/(filesystem::path&& lhs, const std::string& rhs)
{
  return lhs /= rhs;
}

inline filesystem::path operator/(filesystem::path&& lhs, const char* rhs)
{
  return lhs /= rhs;
}
*/

#endif

