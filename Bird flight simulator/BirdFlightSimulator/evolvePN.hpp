#ifndef EVOLVEPN_HPP_INCLUDED
#define EVOLVEPN_HPP_INCLUDED

#include <vector>
#include "glmfwd.hpp"
#include <string>
#include <iterator>


class EvolveBase
{
public:
  virtual ~EvolveBase() {}
  virtual void Reset() = 0;
  virtual void apply() = 0;
  virtual void save(const char* fname, bool append) const = 0;
};


class EvolvePN : public EvolveBase
{
public:
  EvolvePN();
  ~EvolvePN() override {}
	void Reset() override;
	void apply() override;
	void save(const char* fname, bool append) const override;
  int getGeneration() const { return Generation_; };

private:
	void Shuffle();
	void PrepareSave(); 
	double lastShuffle_;

  int Generation_;
	typedef std::vector<float> one_allele;
	typedef std::vector<one_allele>   allele_type;
	typedef std::vector<glm::vec4>   allele_type2;
	typedef std::vector<allele_type2> alleles_vect2;
	typedef std::vector<allele_type> alleles_vect;
	alleles_vect alleles_;
	std::vector<allele_type2> positionsAndSpeed_;
	alleles_vect2 allPandS_;
	alleles_vect2 allPandSPrey_;
	typedef std::vector<float> data_per_pred;
	typedef std::vector<data_per_pred> data_all_pred;
	typedef std::vector<data_all_pred> data_all_generations;
	data_all_generations save_data_;
	std::vector<std::string> names_;
	std::vector<std::string> namesParameters_;
	std::vector<float> ValuesParameters_;
	std::vector<std::string> StringParameters_;
};


#endif
