#ifndef IOTEXTURE_HPP_INCLUDED
#define IOTEXTURE_HPP_INCLUDED

#include <string>
#include <vector>
#include <glsl/texture.hpp>
#include "glmfwd.hpp"


glsl::texture LoadTextureRGB(const std::string& FileName, bool mipmaps, bool compress, glm::ivec2* pExt = nullptr);
glsl::texture LoadTextureRGBA(const std::string& FileName, bool mipmaps, bool compress, glm::ivec2* pExt = nullptr);

// Filenames expected to be +X,-X,+Y,-Y,+Z,-Z
glsl::texture LoadCubeMapTexture(const std::vector<std::string>& FileNames, bool compress);


#endif
