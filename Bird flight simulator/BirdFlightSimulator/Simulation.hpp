#ifndef SIMULATION_HPP_INCLUDED
#define SIMULATION_HPP_INCLUDED

#include <string>
#include <vector>
#include <memory>
#include <iostream>
#include <luabind/luabind.hpp>
#include "glmfwd.hpp"
#include "Params.hpp"
#include "Prey.hpp"
#include "Predator.hpp"
#include "evolvePN.hpp"


class Simulation
{
public:
  using prey_collection = std::vector<std::unique_ptr<CPrey>>;
  using pred_collection = std::vector<std::unique_ptr<CPredator>>;

public:
  Simulation();
  ~Simulation();

  //Robin
  std::unique_ptr<EvolveBase> evolution;
  bool done_;
  double timeSinceEvolution;
  int Generation_;
  
  void SetInitialParameter(const Param::Params&);
  void Initialize_birds();
  void SetParams(const Param::Params&);
  void GetExperimentSettings(const luabind::object& obj);
  void SetPRoost(const Param::Roost&);
  void SetPRenderFlags(const Param::RenderFlags&);
  void Simulation::next_experiment();
  void RegisterFactories(const luabind::object&, const luabind::object&);
  void RegisterDataStorage(const luabind::object&);
  void RegisterEvolution(const luabind::object&);
  luabind::object& GetActiveCamera();
  void SetActiveCamera(const luabind::object& luaobj);
  const Param::Params& Params() const { return params_; }

  // ?????????? Members of Simulation ????????
  void PrintVector(glm::vec3 input, std::string text);
  void PrintVec2(glm::vec2 input, std::string text);
  void PrintFloat(float input, std::string text);
  void PrintString(std::string text);

  void SetFocalBird(const class CBird* bird, bool showTrail);
  void SetPredatorTarget(const class CBird*);
  class CBird* FindById(int id) const;
  class CBird* PickById(int id, bool showTrail);
  class CBird* PickNearestBird2Ray(const glm::vec3& ray_position, const glm::vec3& ray_direction);
  bool HandleKey(unsigned key, unsigned keystate);
  void setNumPrey(unsigned newNum);
  void setNumPredators(unsigned newNum);
  unsigned getNumPrey() const;
  unsigned getNumPredators() const;
  double SimulationTime() const { return SimulationTime_; }   //!< elapsed simulation time since program started (seconds)
  double UpdateTime() const { return UpdateTime_; }           //!< smoothed update time
  double FrameTime() const { return FrameTime_; }             //!< smoothed last frame time
  void main(int argc, char** argv);

  // access to the birds
  const prey_collection& prey() const { return prey_; }
  const pred_collection& pred() const { return pred_; }
  prey_collection& prey() { return prey_; }
  pred_collection& pred() { return pred_; }

  // next bird id to apply
  int nextID() const { return nextID_; }

public:
  Simulation&                sim() { return *this; }
  class GLSLState&           gl() const { return *(gl_); }
  class ICamera const&       ccamera() const { return *camera_; }
  class trail_buffer_pool&   trails() const { return *(trails_); }

  //experiments
   int catch_;
  std::vector <Param::Experiment> experiments_;
  int expNumb_ = 0.0f;
  //! Some more info about the flock
  float meanN_ = 0.0f;
  float meanStartAltitude_ = 0.0f;
  float meanXDist_ = 0.0f;
 
  //data storage
  luabind::object StorageData_;
  luabind::object evolution_next_;

private:
  void UpdateBirds(const float sim_dt);
  void UpdateSimulation(double sim_dt);
  void UpdateSimulationNoGraphicsNoFlock(double sim_dt);

private:
	// Callbacks Window class
  void OnSize(int height, int width);
  void OnLButtonDown(int x, int y, unsigned ks);
  void OnLButtonUp(int x, int y);
  void OnLButtonDblClk(int x, int y);
  void OnMouseMove(int dx, int dy, bool LButtonPressed);
  void OnContextMenu(int menuEntry);

private:
  void EnterGameLoop();
  void EnterGameLoopNoGraphicsNoLua();

private:
  double  SimulationTime_;
  double  UpdateTime_;
  double  FrameTime_;

  prey_collection prey_;
  pred_collection pred_;

  std::unique_ptr<class trail_buffer_pool>  trails_;
  std::unique_ptr<class GLSLState>          gl_;
  class ICamera*                            camera_;
  luabind::object                           luaCamera_;

  bool  statisticsPaused_;
  bool  paused_;

  // Parameter records
  Param::Params params_;
  int nextID_;

  // Registered callbacks
  luabind::object PreyFactory_;
  luabind::object PredatorFactory_;
  
  friend class GLWin;
  friend class LuaStarDisplay;
};


#endif
