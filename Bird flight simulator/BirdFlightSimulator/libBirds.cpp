#include <luabind/luabind.hpp>
#include <luabind/operator.hpp>
#include "Params.hpp"
#include "libParam.hpp"
#include "Bird.hpp"
#include "Prey.hpp"
#include "Predator.hpp"
#include "Globals.hpp"
#include <luabind/adopt_policy.hpp>
#include <luabind/iterator_policy.hpp>

using namespace Param;
using namespace libParam;
using namespace luabind;


namespace libBirds {

  void SetForward(CBird& bird, const glm::vec3& newVal)
  {
    bird.B_[0] = glmutils::save_normalize(newVal, bird.B_[0]);
    bird.velocity_ = bird.speed() * bird.B_[0];
    bird.B_[1] = glm::vec3(0,1,0);
    bird.B_[2] - glm::cross(bird.B_[0], bird.B_[1]);
  }

}


void luaopen_libBirds(lua_State* L)
{
  module(L)[
	  class_<CPredator::hunt>("__predhunt")
      .def_readonly("sequences", &CPredator::hunt::sequences)
      .def_readonly("locks", &CPredator::hunt::locks)
      .def_readonly("catches", &CPredator::hunt::success)
      .def_readonly("minDist", &CPredator::hunt::minDist)
  	  .def_readonly("velocityMinDist", &CPredator::hunt::velocityMinDist)
      .def_readonly("minDistLockedOn", &CPredator::hunt::minDistLockedOn)
      .def_readonly("seqTime", &CPredator::hunt::seqTime)
	    .def_readonly("InterceptionState", &CPredator::hunt::InterceptionState)
      .def_readonly("lockTime", &CPredator::hunt::lookTime),

    class_<CBird>("__bird")
		  .def("isPrey", &CBird::isPrey)
      .property("BirdParams", (Param::Bird& (CBird::*)()) &CBird::GetBirdParams, &CBird::SetBirdParams)
      .def_readwrite("position", &CBird::position_)
      .property("forward", &CBird::forward, &libBirds::SetForward)
      .property("up", &CBird::up)
      .property("side", &CBird::side)
      .property("B", &CBird::B)
      .property("H", &CBird::H)
      .property("velocity", &CBird::velocity_, &CBird::SetVelocity)
      .property("speed", &CBird::speed, &CBird::SetSpeed)
      .def_readonly("force", &CBird::force_)
      .def_readonly("acceleration", &CBird::accel_)
      .def_readwrite("gyro", &CBird::gyro_)
      .def_readwrite("steering", &CBird::steering_)
      .property("id", (int (CBird::*)()) &CBird::id)
      .def("SetTrail", (void (CBird::*)(bool)) &CBird::setTrail)
      .def("HasTrail", (bool (CBird::*)()) &CBird::hasTrail)
      .def("reactionTime", &CBird::reactionTime)
      .def("reactionInterval", &CBird::reactionInterval),
    
    class_<CPrey, CBird>("__prey")
      .property("PreyParams", (Param::Prey& (CPrey::*)()) &CPrey::GetPreyParams, &CPrey::SetPreyParams)
      .def_readonly("detectedPredator", &CPrey::associatedPredator_)
	    .def_readwrite("average_lat_acceleration_", &CPrey::average_lat_acceleration_)
	    .def_readwrite("minDist_pred_", &CPrey::minDist_pred_)
	    .def_readwrite("average_roll_rate_", &CPrey::average_roll_rate_)
	    .def_readwrite("average_out_of_bounds_", &CPrey::average_out_of_bounds_),
    
    class_<CPredator, CBird>("__predator")
      .property("PredParams", (Param::Predator& (CPredator::*)()) &CPredator::GetPredParams, &CPredator::SetPredParams)
      .def("GetTargetPrey", (CPrey* (CPredator::*)()) &CPredator::GetTargetPrey)
      .def("SetTargetPrey", (void (CPredator::*)(const CPrey*)) &CPredator::SetTargetPrey)
      .def("GetHuntStat", (CPredator::hunt* (CPredator::*)()) &CPredator::hunts)
      .def("ResetHunt", (void (CPredator::*)()) &CPredator::ResetHunt)
      .def("EndHunt", &CPredator::EndHunt)
      .def("StartAttack", (void (CPredator::*)()) &CPredator::BeginHunt)
  ];
}

