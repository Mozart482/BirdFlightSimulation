#include <random>
#include <glmutils/ray.hpp>
#include <glmutils/random.hpp>
#include "random.hpp"
#include "Bird.hpp"
#include "Globals.hpp"
#include <stdlib.h> 
#include <iostream>
#include <math.h>

using namespace Param;

const float pi = 3.14159265358979323846f;

CBird::CBird(int id, const glm::vec3& position, const glm::vec3& forward)
: position_( position ),
  wingSpan_(0.5f),
  velocity_(0),
  speed_(-1),
  accel_(0),
  force_(0),
  gyro_(0),
  random_orientation_(0),
  beatCycle_(0),
  flightForce_(0),
  reactionTime_(0),
  StoreTrajectory_(0),
  reactionInterval_(0),
  trail_(nullptr),
  id_(id)
{
  B_[0] = forward;
  B_[2] = glmutils::save_normalize(glm::cross(B_[0], B_[1]), glm::normalize(glm::cross(B_[0] + 0.1f, B_[1])));
  B_[1] = glm::cross(B_[2], B_[0]);
  H_ = B_;
}


CBird::~CBird()
{
  GTRAILS.destroy(trail_);
}


void CBird::SetBirdParams(const Param::Bird& pBird)
{
  pBird_ = pBird;
  wingSpan_ = pBird.wingSpan;
  reactionTime_ = std::uniform_real_distribution<float>()(rnd_eng()) * pBird.reactionTime;
  if (speed_ < 0.0f) 
  {
    speed_ = pBird_.cruiseSpeed;
    velocity_ = speed_ * B_[0];
  }
}


void CBird::SetSpeed(float x)
{
  speed_ = std::max<float>(x, 0.01f);
  velocity_ = speed_ * B_[0];
}


void CBird::SetVelocity(glm::vec3 const& x)
{
  velocity_ = x;
  speed_ = glm::length(velocity_);
}


void CBird::RoostChanged()
{
}


void CBird::setTrail(bool show)
{
  if (show && trail_) return;   // done
  if (nullptr == trail_)
  {
    trail_ = GTRAILS.create(id_);
  }
  else
  {
    GTRAILS.destroy(trail_);
    trail_ = nullptr;
  }
}




void CBird::induceControlError()
{
	if (pBird_.errorInControl > 0.0001f)
	{
		if (abs(desiredLift_ - prevDesiredLift_) > 0.0001f)
		{
			std::normal_distribution<> norm_dist(0.0, abs(desiredLift_ - prevDesiredLift_) * pBird_.errorInControl);
			controlError_ = static_cast<float>(norm_dist(rnd_eng()));
			prevDesiredLift_ = desiredLift_;
		}
	}
	if (pBird_.errorInRoll > 0.0001f)
	{
		if (abs(turn_) > 0.0001f)
		{
			std::normal_distribution<> norm_dist(0.0, abs(turn_) * pBird_.errorInRoll);
			roll_error_ = static_cast<float>(norm_dist(rnd_eng()));
		}
	}
}


void CBird::flight_dynamic(float sim_dt)
{
	const float pi = glm::pi<float>();
	//debug mode:
	if (pBird_.contMaxLift > 0.001) desiredLift_ = std::min(desiredLift_, pBird_.contMaxLift);

	//LEV can be set greater than zero to account for low speed flight cl (pennycuick)
	float LEV = 3.0f;
	float clmax = pBird_.wingBeatFreq * pBird_.wingBeatFreq / (pBird_.wingBeatFreq*pBird_.wingBeatFreq + speed_*speed_) * LEV + 1.6f;
	const float dynamic = 0.5f*pBird_.rho*speed_* speed_;
	// subtract the part of the body of total wing area
	float area = pBird_.wingArea * (pBird_.wingLength * 2) / pBird_.wingSpan;

	//Gliding
	float bmax = pBird_.wingSpan;
	float bmin = pBird_.wingSpan -2 * pBird_.wingLength;
	float L0 = pBird_.L0;
	float b_maxlift = std::min(bmax, (bmax - bmin) * std::sqrt(L0 / std::max((clmax*dynamic*area), 0.00001f)) + bmin);
	float lift_Max_gliding = clmax *dynamic*area* (b_maxlift - bmin) / (bmax - bmin);
	float r_desired_lift = std::min(desiredLift_, lift_Max_gliding);
	// calculate the optimal wingspan, to decrease drag, given certain lift
	float b = bmax;
	if (speed_ > 0.00001f) b = pow((bmax - bmin) * 8.0f * r_desired_lift * r_desired_lift / (pi * pBird_.cFriction*area*(pBird_.rho*speed_*speed_)*(pBird_.rho*speed_*speed_)), 1.0f / 3.0f);
	// bound the wingspan to min and max
	b = std::min(std::max(b, bmin), bmax);
	//also bound it such that the generated torque does not exceed 1.7*W*bmax
	float max_b = L0 / r_desired_lift * bmax;
	b = std::min(b, max_b);
	// calculate CL given b
	float CL_glide = std::min(clmax, r_desired_lift / std::max((dynamic * area* (b - bmin) / (bmax - bmin)), 0.00001f));
	// make sure with the new CL, the new b is also still smaller than maximum wing span
	b = std::min(r_desired_lift / std::max((dynamic * area* CL_glide / (bmax - bmin)), 0.00001f) + bmin, bmax);
	// calculate new area
	float area_glide = area * (b - bmin) / (bmax - bmin);
	// calculate new aspect ratio
	float AR_glide = b*b / area_glide;
	// calculate new drag 
	float D_glide = pBird_.cBody * dynamic *pBird_.bodyArea + pBird_.cFriction * dynamic * area_glide + CL_glide*CL_glide * area_glide*dynamic / (pi * AR_glide);
	// set lift and drag
	float L_gliding = CL_glide *dynamic *area_glide;
	float TD_gliding = -D_glide;

	//flapping
	float CL = std::min(r_desired_lift / std::max((dynamic*area), 0.00001f), clmax);
	// unconstrained Tmax
	float Tmax = pi*pi*pi / 16.0f * pBird_.rho * pBird_.wingAspectRatio * area * (sin(0.5f*pBird_.theta)*pBird_.wingBeatFreq)  * (sin(0.5f*pBird_.theta)*pBird_.wingBeatFreq) * pBird_.wingLength *  pBird_.wingLength;
	// constrained Tmax
	float Tmax_cons = std::min(Tmax, Tmax * pBird_.cruiseSpeed / std::max(speed_, 0.00001f));
	// calculate Thrust for flapping. 
	float T_flapping = -CL*CL * area * dynamic / (pi * pBird_.wingAspectRatio) + (1 - CL*CL / (clmax*clmax))*Tmax_cons;
	// and lift
	float L_flapping = CL *dynamic *area;
	// calculate the body drag and friction drag
	float D_flapping = pBird_.cBody * dynamic *pBird_.bodyArea + pBird_.cFriction * dynamic * area;
	// net result of thrust and drag for flapping
	float TD_flapping = T_flapping - D_flapping;

	//angular acceleration
	float liftMax = clmax *dynamic*pBird_.wingArea* (b_maxlift - bmin) / (bmax - bmin);
	// calculate the inertia, bases on b_maxlift
	float phi = (b_maxlift - bmin) / (bmax - bmin);
	float InertiaWingCenter = pBird_.InertiaWing * phi*phi + 1.0f / 4.0f * 0.098f*0.098f * pow(pBird_.bodyMass, 0.70f) * pBird_.wingMass + 0.098f * pow(pBird_.bodyMass, 0.35f) * pBird_.J*phi;
	float Inertia = 2.0f * InertiaWingCenter + pBird_.InertiaBody;
	//calculate roll acceleration with inertia and b maxlift
	angular_acc_ = liftMax * (b_maxlift / 4) / (Inertia) / 2;
	//debug mode:
	if (pBird_.constRollAcc > 0.001 && angular_acc_ > pBird_.constRollAcc) angular_acc_ = pBird_.constRollAcc;
	if (pBird_.NoIndDrag == true)
	{
		TD_gliding = -pBird_.cBody * dynamic *pBird_.bodyArea - pBird_.cFriction * dynamic * area;
		TD_flapping = (1 - CL*CL / (clmax*clmax))*Tmax_cons - D_flapping;
	}

	//compute quantities
	float L = CL *dynamic *area;
	lift_ = L*B_[1];
	liftMax_ = liftMax*B_[1];
	if (TD_flapping > TD_gliding) glide_ = false; else glide_ = true;
	float thrust_drag = std::max(TD_flapping, TD_gliding);
	// debug mode:
	if (pBird_.oscillations > 0.001f) thrust_drag += sin(float(SIM.SimulationTime()) * 24.0f) * pBird_.oscillations * pBird_.bodyMass;
	flightForce_ = lift_ + B_[0] * (thrust_drag);
	flightForce_.y -= pBird_.bodyMass * 9.81f;        // apply gravity
	if (glide_ == true) span_ = (b - 0.12) / (bmax - 0.12); else span_ = 1.0f;
	forACC_ = thrust_drag / pBird_.bodyMass;
	latACC_ = L / pBird_.bodyMass;
	desLatACC_ = desiredLift_ / pBird_.bodyMass;
	integration(sim_dt);
}


// Verlet integration
void CBird::integration(float dt)
{
  const float hdt = 0.5f * dt;
  const float rBM = 1.0f / pBird_.bodyMass;
  glm::vec3 forward(B_[0]);

  velocity_ += accel_ * hdt;                 // v(t + dt/2) = v(t) + a(t) dt/2
  position_ += velocity_ * dt;               // r(t + dt) = r(t) + v(t + dt/2)
  accel_ = (flightForce_)* rBM;              // a(t + dt) = F(t + dt)/m
  velocity_ += accel_ * hdt;                 // v(t) = v(t + dt/2) + a(t + dt) dt/2
  speed_ = glm::length(velocity_);

  if (speed_ > 0.00001f) forward = velocity_ / speed_;
  B_[0] = forward;
}


void CBird::regenerateLocalSpace(float dt)
{
	glm::vec3 forward = B_[0];
	glm::vec3 up = B_[1];
	glm::vec3 side = B_[2];
	glm::vec3 steering = steering_;
	steering.y += pBird_.bodyMass * 9.81f;
	glm::vec3 bank_des = glm::vec3(0.0f, glm::dot(steering, up), glm::dot(steering, side));
	glm::vec3 bank_cur = glm::vec3(0.0f, 1.0f, 0.0f);
	float turn = asin(glm::cross(bank_cur, bank_des).x / (glm::length(bank_cur) * glm::length(bank_des)));
	//set commanded lift
	desiredLift_ = std::max(glm::length(bank_des) + controlError_, 0.0f);
	// calculate roll acceleration via bang-bang cnotrol. 
	float p = turn;
    float a = angular_acc_;
	float c = roll_rate_;
	if (p < 0) a *= -1;
	if (c*c > 2 * a*p && p*c > 0) a *= -1;
	//adjust the roll rate
	roll_rate_ = roll_rate_ + a*dt;
	//avoid spinning of crazy roll rates
	if (roll_rate_ > 300.0f) roll_rate_ = 300.0f;
	//adjust body orientation
	forward = glm::normalize(forward);
	up += roll_rate_* dt*side;
	side = glm::normalize(glm::cross(forward, up));
	up = glm::cross(side, forward);

	B_[0] = forward;
	B_[1] = up;
	B_[2] = side;

	// Head system
	up = glm::vec3(0, 1, 0);            // tmp. Head-up
	side = glm::normalize(glm::cross(forward, up));   // Head-side
	up = glm::cross(side, forward);   // Head-up
	H_[0] = forward;
	H_[1] = up;
    H_[2] = side;

	//wing beat graphics (graphical output only)
	beatCycle_ += dt*(pBird_.wingBeatFreq * 2.0f * pi);
	if (glide_) beatCycle_ = 0.25f*pBird_.wingBeatFreq*2.0f * pi;
	if (SIM.SimulationTime() < 0.1) beatCycle_ += rand_;
	float Rs = 0.80;
	float t = fmod(beatCycle_ / (2.0f*3.14f), 1);
	float retract_time = 0.5f / 2.0f*1.5f;
	float extension_time = 0.375f;
	RS_ = float(t < 0.25 && t >= 0.175);
	RS_ += float((t >= 0.25) && (t < (0.25 + retract_time)))*(Rs + (1 - Rs)*(1 + cos(pi*(t - 0.25) / retract_time)) / 2);
	RS_ += float(t >= (0.25 + retract_time) & t < 0.8)*Rs;
	RS_ += float(t >= 0.8 & t <= (0.8 + extension_time))*(Rs + (1 - Rs)*(1 - (1 + cos(pi*(t - 0.8) / extension_time)) / 2));
	RS_ += float(t<0.175)*(Rs + (1 - Rs)*(1 - (1 + cos(pi*(t - 0.8 + 1) / extension_time)) / 2));
	if (glide_) RS_ = span_;
}

void CBird::nextReactionTime()
{
  reactionTime_ = 0.0f;
  float rs = 0.0f;
  reactionInterval_ = pBird_.reactionTime * (1.0f  + (rs == 0.0f ? 0.0f : std::uniform_real<float>(-rs, +rs)(rnd_eng())));
}
