//! \file GLSLModel.hpp 3D triangulated models.
//! \defgroup Models Triangulated 3D Models for OpenGL  

#ifndef GLSLMODEL_HPP_INCLUDED
#define GLSLMODEL_HPP_INCLUDED

#include <memory>
#include <string>
#include <array>
#include <vector>
#include <glsl/glsl.hpp>
#include <glsl/buffer.hpp>
#include <glsl/vertexarray.hpp>
#include "ac3d.hpp"
#include "GLSLiotexture.hpp"
#include "glmfwd.hpp"
#include "Params.hpp"

class GLSLModel;


//! \return 3D Model
//! \ingroup Models Visualization
std::unique_ptr<GLSLModel> CreateModel(unsigned ModelId);


//! Base class for OpenGL models.
//! \ingroup Models Visualization
class GLSLModel
{
public:
  //! Constructs 3D model from .ac file
  GLSLModel(const Param::ModelDef& modelDef);
  ~GLSLModel();

  void Bind();            //! bind OpenGL state 
  void Unbind();          //! unbind OpenGL state
  const ac3d_material& material() const { return material_; }
  float modelScale() const { return modelScale_; }  //! return model scale
  const unsigned nVertices() const { return nVertices_; }
  const unsigned nIndices() const { return nIndices_; }

private:
  unsigned            nVertices_;
  unsigned            nIndices_;
  bool                twoSided_;
  ac3d_material       material_;
  glsl::texture       texture_;
  glsl::buffer        vbuffer_;
  glsl::buffer        ibuffer_;
  glsl::vertexarray   vao_;
  float               modelScale_;
};


#endif
