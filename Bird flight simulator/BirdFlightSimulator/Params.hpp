#ifndef BFS_PARAMS_HPP_INCLUDED
#define BFS_PARAMS_HPP_INCLUDED

#include <string>
#include <vector>
#include <glm/glm.hpp>
#include <luabind/luabind.hpp>


namespace Param {

  struct Roost
  {
    unsigned numPrey;
    unsigned numPredators;
    float Radius;
    float minRadius;
    float maxRadius;
  };


  struct Trajectory
  {
	  glm::vec3 Pred_position;
	  glm::vec3 Prey_position;
	  glm::vec3 Pred_up;
	  glm::vec3 Prey_up;
	  glm::vec3 Pred_forward;
	  glm::vec3 Prey_forward;
	  glm::vec3 Pred_acc;
	  glm::vec3 Prey_acc;
	  glm::vec3 Pred_velocity;
	  glm::vec3 Prey_velocity;
	  float Prey_forACC;
	  float Prey_latACC;
	  float Pred_desLatACC;
	  float Pred_latACC;
	  float Pred_roll_rate;
	  float Pred_roll_acc;
	  float Prey_roll_rate;
	  float Prey_roll_acc;
	  int Pred_id;
	  int Pred_gen;
	  int Prey_id;
	  float N;
	  double time;
  };

  struct DataStorage
  {
	  std::string folder;
  };

  
  struct Birds
  {
	  std::string csv_file_species;
	  std::string csv_file_prey_predator_settings;
  };

  struct Trajectories
  {
	  int amount;
	  float dt;
  };

  struct Evolution
  {
	  std::string type;
	  std::string fileName;
	  float durationGeneration;
	  int startGen;
	  bool load;
	  std::string loadFolder;
	  Trajectories Trajectories;
	  int terminationGeneration;
	  bool stationaryPrey;
	  bool artificialPrey;
	  bool artificialPredator;
	  std::string externalPreyFile;
	  std::string title;
	  bool oneOnOnePredPrey;
  };

   
  struct ModelDef { 
    std::string name;
    std::string acFile;
    std::string shader;
    int id;
    float Scale;
  };


  struct Skybox {
    std::string name;
    glm::vec3 ColorCorr;
    float fovy;
  };


  struct Bird {
    float reactionTime;
    float rho;
    float bodyMass;
	  float wingMass;
	  float InertiaWing;
	  float InertiaBody;
	  float J;
    float bodyWeight;
    float wingSpan;
    float wingAspectRatio;
    float wingArea;
	  float wingBeatFreq;
	  std::string birdName; 
	  float theta;
	  float wingLength;
	  float bodyArea;
	  float cBody;
	  float cFriction;
	  float constRollAcc;
	  float L0;
	  float contMaxLift;
	  float oscillations;
    float CL;
	  int generation;
    float cruiseSpeed;
    float speedControl;
    float blindAngle;
    float altitude;
	  int maneuver;
	  glm::vec3 InitialPosition;
	  glm::vec3 InitialHeading;
	  float InitialSpeed;
	  float TopSpeed;
	  float errorInControl; 
	  float errorInRoll;
	  bool NoIndDrag; 
  };


  struct Prey {

    enum EvasionStrategies
    {
		//! important: when adding new evasion strategies, don't forget to change the names as well!
      MaximizeDist,     // maximize distance of closest approach
      TurnInward,       // turn along circularity vector
      TurnAway,         // Turn in opposite direction (Chris)
      Drop,             // Drop out of sky
			MoveCentered,			// Move towards center
      Zig,              // Left-Right evasion. Parameter edge is reinterpreted as (TirgDist, t_left, t_right, t_handle)
      Custom,	  			  // Lua
      MaxEvasionStrategy__
    };

	  int EvasionStrategyTEMP;
	  glm::vec4 smoothManeuverParam;
	  glm::vec4 nonSmoothManeuverParam;
	  glm::vec4 noManeuverParam;
	  glm::vec3 artificial;
	  glm::vec3 fleeDirection;
	  glm::vec4 startingPoints;
	  glm::vec4 endPoints;
	  glm::vec4 z_evade;
	  glm::vec4 y_evade;
  };


  struct Pursuit {
    Pursuit() : type(0), deflection(0) {}
    int type;
    glm::vec3 deflection;
    luabind::object hook;
  };


  struct Predator {
    enum StartAttacks
    {
      Manual = 0,
      Auto = 1,
      Evolve = 2,
      MaxAttackStrategy__
    } StartAttack;

    enum PreySelections
    {
      Topo = 0,
      Picked = 1,
      PickedTopo = 2,
      MaxPreySelection__
    } PreySelection;

   
	  int PursuitStrategy;
    float CatchDistance;
	  bool StoreTrajectory;
	  float VisualError;
	  glm::vec3 artificial;
	  glm::vec2 VisualBias;	
	  float DPAdjParam;
	  float N;
	  float N2;
	  int filter;
  };

  struct RenderFlags {
    bool show_trails;
    bool show_annotation;
    bool show_fps;
    bool slowMotion;
    std::string helpMsg;
	  bool turnOffGraphics;
  };

  struct Params 
  {
    int DebugLevel;
    bool DebugLogOnce;
    int DebugLogStackLevel;
    int FSAA[2];
    int swap_control;
    double IntegrationTimeStep;
    int slowMotion;
    int pausedSleep;
    bool realTime;
    int maxSkippedFrames;
    unsigned maxPrey;
    unsigned maxPredators;
    unsigned maxTopologicalRange;
    Roost roost;
	  DataStorage DataStorage;
    std::vector<std::pair<std::string, std::string> > Fonts;  // name filename pair
    glm::vec3 TextColor;
    glm::vec3 TextColorAlt;
    Skybox skybox;
    std::vector<ModelDef> ModelSet;
    float RulerTick;
    unsigned RulerMinFlockSize;
    double TrailLength;
    float TrailWidth;
    float TrailTickInterval;
    float TrailTickWidth;
    int   TrailSkip;
    RenderFlags renderFlags;
	  Evolution evolution;
	  Birds     birds;
  };

  
  struct Experiment
  {
	  Params param;
	  Bird preyBird;
	  Bird predBird;
	  Predator pred;
	  Prey prey;
  };

}


#endif
