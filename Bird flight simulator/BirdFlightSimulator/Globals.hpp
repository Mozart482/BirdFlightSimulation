#ifndef GLOBALS_HPP_INCLUDED
#define GLOBALS_HPP_INCLUDED

#include <atomic>
#include <atlbase.h>
#include "libStarDisplay.hpp"
#include "Simulation.hpp"


extern class LuaStarDisplay Lua;
extern class GLWin* AppWin;
extern class Simulation* Sim;
extern std::atomic<bool> SimulationQuitFlag;


struct Globals
{
  Globals();
  ~Globals();
};


#define LUA (Lua)
#define LUASIM Lua.LuaSim()

#define APPWIN (*AppWin)
#define SIM (*Sim)
#define PARAMS SIM.Params()
#define PROOST SIM.Params().roost
#define PSKYBOX SIM.Params().skybox
#define PRENDERFLAGS SIM.Params().renderFlags

#define GGl SIM.gl()
#define GTEXT GGl.Fonts
#define GCAMERA SIM.ccamera()      // const
#define GTRAILS SIM.trails()


#endif
