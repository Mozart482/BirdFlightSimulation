//! \file Predator.h The Predator
//! \ingroup Model

#ifndef PREDATOR_HPP_INCLUDED
#define PREDATOR_HPP_INCLUDED


#include "Bird.hpp"


class CFlock;
class CPrey;


//! Predator class
//! \ingroup Simulation
class CPredator : public CBird
{
  CPredator();
  CPredator(const CPredator&) = delete;
  CPredator& operator=(const CPredator&) = delete;
  CPredator(CPredator&&) = delete;
  CPredator& operator=(CPredator&&) = delete;

public:
  struct hunt
  {
    hunt() : sequences(0), locks(0), success(0),
             minDist(999.99f), minDistLockedOn(999.99f), 
			       seqTime(0), lookTime(0), lastMinDist(999.99f)
    {}
    hunt& operator+=(const hunt& h);
    int sequences;
    int locks;
    int success;
    float minDist;
   	float velocityMinDist;
    float minDistLockedOn;
	
    double seqTime;
    double lookTime;
	  Param::Trajectory InterceptionState;
	  std::vector<Param::Trajectory> Trajectory_;
    std::vector<unsigned> attackSize;
    std::vector<unsigned> catchSize;
	  //! add the miss distance of last attack too
    float lastMinDist;
  };

public:
  CPredator(int ID, const glm::vec3& position, const glm::vec3& forward);
  virtual ~CPredator();

  virtual bool isPrey() const { return false; }
  virtual bool isPredator() const { return true; }
  
  virtual void NumPreyChanged();
  virtual void NumPredChanged();

  const Param::Predator& GetPredParams() const { return pPred_; }
  Param::Predator& GetPredParams() { return pPred_; }
  void SetPredParams(const Param::Predator& pPred);

  Param::Trajectory handle_trajectory_storage(bool storeInVector);
  Param::Predator  pPred_;

  void update(float dt);

  const CPrey* GetLockedOn() const { return lockedOn_; }
  const CPrey* GetTargetPrey() const { return targetPrey_; }
  //! bunch of functions to set and get variables
  void setGeneration(float generation) { generation_ = generation; }
  void SetTargetPrey(const CPrey*);
  const glm::vec3& TargetPoint() const { return targetPoint_; }
  const hunt& hunts() const { return hunts_; }
  void BeginHunt();
  void testSettings();
  void EndHunt(bool success);
  void ResetHunt();
  hunt hunts_;
  bool is_attacking_ = true;

  typedef std::vector<glm::vec4>   Storage;
  Storage positionsAndSpeed;

private:
  friend struct find_prey_qf;

  void Accelerate();
  void handle_user_controls();
  void handle_direct_attack();
  void flightDynamic_artificial_predator();

  void proportionalNavigation(const glm::vec3& targetPosition, const glm::vec3& targetVelocity);
  void augProportionalNavigation(const glm::vec3& targetPosition, const glm::vec3& targetVelocity, const glm::vec3& targetAcceleration);
  void PNDP(const glm::vec3& targetPosition, const glm::vec3& targetVelocity);
  void DirectPursuit(const glm::vec3& targetPosition, const glm::vec3& targetVelocity);
  void DirectPursuit2(const glm::vec3& targetPosition, const glm::vec3& targetVelocity);
  void checkEndHunt(const glm::vec3& targetPosition);
  void proportionalNavigation_error_estimation_velocity(const glm::vec3& targetPosition, const glm::vec3& targetVelocity);
  void proportional_navigation_kalman_filtered(const glm::vec3& targetPosition, const glm::vec3& targetVelocity);
  glm::vec3 addVisualError(const glm::vec3& relative_position);

  //kalman functions
  void observe(const glm::vec3& relative_position);
  void predict(void);
  void predict_extended_kalman_filter(void);
  void update(void);

//  float            attackTime_;

  float            handleTime_;
  float            dogFight_;
  const CPrey*     lockedOn_;
  const CPrey*     closestPrey_;
  const CPrey*     targetPrey_;
  glm::vec3        targetPoint_;
  glm::mat3 obs_cov_ = glm::mat3(0.0f);
  glm::mat3 untracked_cov_ = glm::mat3(0.0f);
  glm::mat3 P_ = glm::mat3(0.0f);
  glm::mat3 P_prev_ = glm::mat3(0.0f);
 
  int              locks_;        // locks in current attack
  //! some new parameters, probably will move them to params
  float            generation_ = 0.0f;
  glm::vec3 previous_relative_position_;
public:
  glm::vec3 r_prev_ = glm::vec3(0.0f);
  glm::vec3 v_prev_ = glm::vec3(0.0f);
  glm::vec3 a_t_prev_ = glm::vec3(0.0f);
  glm::vec3 r_obs_ = glm::vec3(0.0f);
  glm::vec3 v_obs_ = glm::vec3(0.0f);
  glm::vec3 a_t_obs_ = glm::vec3(0.0f);
  glm::vec3 r_est_ = glm::vec3(0.0f);
  glm::vec3 v_est_ = glm::vec3(0.0f);
  glm::vec3 a_t_est_ = glm::vec3(0.0f);
  glm::vec3 r_real_ = glm::vec3(0.0f);
  glm::vec3 v_real_ = glm::vec3(0.0f);
  glm::vec3 a_real_ = glm::vec3(0.0f);
  glm::vec3 steering_prev_ = glm::vec3(0.0f);
private:
  typedef void (CPredator::*handleAttackMPTR)();
  typedef void (CPredator::*selectPreyMPTR)(struct find_prey_qf&);
  static handleAttackMPTR startAttack[Param::Predator::MaxAttackStrategy__];
  static selectPreyMPTR selectPrey[Param::Predator::MaxPreySelection__];
};



#endif
