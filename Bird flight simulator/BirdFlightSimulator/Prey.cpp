#include <cassert>
#include <limits>
#include <glm/gtx/projection.hpp>
#include <glm/gtx/fast_square_root.hpp>
#include <glm/gtx/transform.hpp>
#include <glmutils/random.hpp>
#include <glmutils/homogeneous.hpp>
#include <glmutils/clip_length.hpp>
#include <glmutils/plane.hpp>
#include <glmutils/ray.hpp>
#include <glmutils/matrix_norm.hpp>
#include <glmutils/smootherstep.hpp>
#include "glmfwd.hpp"
#include "Prey.hpp"
#include "Globals.hpp"
#include <iostream>
#include <string>
#include "random.hpp"
#include "ICamera.hpp"

using namespace Param;

// this is illegal (M_PI is in the posix-standard)
//  # define M_PI           3.14159265358979323846
const float pi = 3.14159265358979323846f;


CPrey::CPrey(int ID, const glm::vec3& position, const glm::vec3& forward)
  : CBird(ID, position, forward),
  associatedPredator_(nullptr),
  flight_dynamic_mem_fn_(&CPrey::flight_dynamic)
{
	random_orientation_ = glm::vec3(1.0f,0.0f,0.0f);
}


CPrey::~CPrey()
{
}


void CPrey::SetPreyParams(const Param::Prey& prey)
{
  pPrey_ = prey;
  // flight dynamic runtime dispatch: only once.
  const auto& evo = SIM.Params().evolution;
  if (evo.stationaryPrey) flight_dynamic_mem_fn_ = &CPrey::flight_dynamic_stationary_prey;
  else if (evo.artificialPrey) flight_dynamic_mem_fn_ = &CPrey::flight_dynamic_artificial_prey;
  else flight_dynamic_mem_fn_ = &CPrey::flight_dynamic;
}



void CPrey::NumPreyChanged()
{
}


void CPrey::NumPredChanged()
{
  associatedPredator_ = nullptr;
}



void CPrey::update(float dt)
{
	calculateAccelerations();
	handleTrajectoryStorage();
	if ((reactionTime_ += dt )>= reactionInterval_)
  {
      steering_ = glm::vec3(0);
	  handleManeuvers(); 
    // calculate time of next reaction
    nextReactionTime();
  }

	if (associatedPredator_ && glm::length(associatedPredator_->position_ - position_) < 0.2)
	{
		position_ = associatedPredator_->position_ - glm::vec3(0,0.05,0);
		velocity_ = associatedPredator_->velocity_;
		glide_ = 1;
		roll_rate_ = 0.0f;
	}
	else
	{
		// call through our member function pointer (note the odd syntax)
		(this->*flight_dynamic_mem_fn_)(dt);
	}

  
  regenerateLocalSpace(dt);
  appendTrail(trail_, position_, B_[2], 0.5f, dt, RS_, pBird_.wingSpan*4.0f, beatCycle_, B_[1]);
  if ((GCAMERA.GetFocalBird())->id() == id_) testSettings();
}



void CPrey::handleTrajectoryStorage()
{
}

void CPrey::handleManeuvers()
{
  // this should be handled with mfp's too...
  // you can even have an array of mfp's.
	if (pBird_.maneuver == 1) steering_ = glmutils::save_normalize(glm::vec3(pBird_.InitialHeading.x, 0.0f, pBird_.InitialHeading.z), glm::vec3(1, 0, 0)) * 20.0f;
	if (pBird_.maneuver == 2) { maneuver_non_smooth(); };
	if (pBird_.maneuver == 3){ maneuver_smooth(); };
	if (pBird_.maneuver == 4){ maneuver_optimal(); };
	if (pBird_.maneuver == 5) { maneuver_non_smooth_adapt(); };
}



void CPrey::flight_dynamic_artificial_prey(float sim_dt)
{
	speed_ = pPrey_.artificial.x;
	lift_ = std::min(desiredLift_, pPrey_.artificial.y * pBird_.bodyMass)*B_[1];
	liftMax_ = pPrey_.artificial.y * pBird_.bodyMass * B_[1];
	forACC_ = 0.0;
	latACC_ = pPrey_.artificial.y;
	flightForce_ = lift_;
	flightForce_.y -= pBird_.bodyMass * 9.81f;
	angular_acc_ = pPrey_.artificial.z;

	velocity_ = glm::normalize(velocity_) * speed_;
  integration(sim_dt);
}


void CPrey::flight_dynamic_stationary_prey(float)
{
  // do nothing
}


//This function models the smooth maneuver type, as described in the supplementary material of the first publication
void CPrey::maneuver_smooth()
{
	float max = glm::sqrt(glm::length(liftMax_)*glm::length(liftMax_) - pBird_.bodyWeight * pBird_.bodyWeight);
	float tmp = (pPrey_.smoothManeuverParam.x*pow(abs(sin(pPrey_.smoothManeuverParam.y*float(SIM.SimulationTime()))), pPrey_.smoothManeuverParam.z) + pPrey_.smoothManeuverParam.x) * max;
	if (!std::isfinite(tmp) || std::isnan(tmp)) tmp = 100;
	steering_ += (H_[2]) *tmp;// +(H_[1]) * (float(rand()) / float(RAND_MAX)) * 0.5f *tmp;
	//Include the altitude control
	//steering_.y += glm::clamp(pPrey_.smoothManeuverParam.w *(pBird_.altitude - position_.y), -pBird_.bodyMass*9.81f * 3, pBird_.bodyMass*9.81f * 3);
	steering_.y += glm::clamp(pPrey_.nonSmoothManeuverParam.w *(pBird_.altitude - position_.y), -pBird_.bodyMass*9.81f * 3, pBird_.bodyMass*9.81f * 3) - glm::clamp(0.2f * sqrt(pPrey_.nonSmoothManeuverParam.w) *(velocity_.y), -pBird_.bodyMass*9.81f * 3, pBird_.bodyMass*9.81f * 3);

}


void CPrey::maneuver_optimal()
{
	float max = 1.20f*glm::sqrt(glm::length(liftMax_)*glm::length(liftMax_) - pBird_.bodyWeight * pBird_.bodyWeight);
	minDist_pred_ = associatedPredator_->hunts_.minDist;
	bool evade = false;
	if (!associatedPredator_) return;
	glm::vec3  posD = associatedPredator_->position_ - position_;
	glm::vec3  vecD = velocity_ - associatedPredator_->velocity_;
	
	float time_impact = glm::dot(posD, posD) / glm::dot(vecD, posD);

	for (int n = 0; n<4; n++)
	{
		if (pPrey_.startingPoints[n] > time_impact && pPrey_.endPoints[n] < time_impact && !evade)
		{
			//SIM.PrintFloat(time_impact, " time left .. ");
			steering_ += (H_[2]) *std::max(std::min(pPrey_.z_evade[n], max),-max);
			steering_ += (H_[1]) *std::max(std::min(pPrey_.y_evade[n],max),-max);
			evade = true;
		}	
	}
	if (evade == false) steering_ = pPrey_.fleeDirection;
	steering_.y += glm::clamp(pPrey_.smoothManeuverParam.w *(pBird_.altitude - position_.y), -pBird_.bodyMass*9.81f * 3, pBird_.bodyMass*9.81f * 3);
}

//This function models the non-smooth maneuver type, as described in the supplementary material of the first publication
void CPrey::maneuver_non_smooth()
{
	float max = glm::length(liftMax_) - pBird_.bodyMass*9.81f / 2.0f;
	if ((float(rand()) / float(RAND_MAX)) < (reactionInterval_ * pPrey_.nonSmoothManeuverParam.x))
	{
		pPrey_.nonSmoothManeuverParam.y = std::min(1.0f, pPrey_.nonSmoothManeuverParam.y);
		glm::vec2 temp(glmutils::unit_vec2(rnd_eng()));
		glm::vec3 new_orientation = glmutils::save_normalize(glm::vec3(temp.x, float(rand()) / float(RAND_MAX) * 1.0f - 0.5f, temp.y), glm::vec3(1, 0, 0));
		random_orientation_ = pPrey_.nonSmoothManeuverParam.y * new_orientation + (1 - pPrey_.nonSmoothManeuverParam.y) * random_orientation_;
	};
	steering_ += random_orientation_ * pPrey_.nonSmoothManeuverParam.z * max;
	//Include the altitude control
	steering_.y += glm::clamp(pPrey_.nonSmoothManeuverParam.w *(pBird_.altitude - position_.y), -pBird_.bodyMass*9.81f * 3, pBird_.bodyMass*9.81f * 3) - glm::clamp(4 * sqrt(pPrey_.nonSmoothManeuverParam.w) *(velocity_.y), -pBird_.bodyMass*9.81f * 3, pBird_.bodyMass*9.81f * 3);

}

void CPrey::maneuver_non_smooth_adapt()
{
	float max = glm::length(liftMax_);
	if ((float(rand()) / float(RAND_MAX)) < (reactionInterval_ * pPrey_.nonSmoothManeuverParam.x))
	{
		float coinflip = int(float(rand()) / float(RAND_MAX) > 0.5);
		random_orientation_ = float(coinflip) * (H_[2]) - (1-float(coinflip)) * (H_[2]);
	};
	steering_ += random_orientation_ * max;
	//Include the altitude control
	steering_.y += glm::clamp(pPrey_.nonSmoothManeuverParam.w *(pBird_.altitude - position_.y), -pBird_.bodyMass*9.81f * 3, pBird_.bodyMass*9.81f * 3) - glm::clamp(4 * sqrt(pPrey_.nonSmoothManeuverParam.w) *(velocity_.y), -pBird_.bodyMass*9.81f * 3, pBird_.bodyMass*9.81f * 3);
}


//! This is probably a slow function to calculate stuff. Perhaps a better way?
void CPrey::calculateAccelerations()
{
	// counter is reset to 0 with each new experiment in EvolvePN
	counter_acc_++;

	if (counter_acc_ == 1)
	{
		max_for_acceleration_ = 0.0f;
		max_lat_acceleration_ = 0.0f;
		max_roll_rate_ = 0.0f;
	}

	average_for_acceleration_ = abs(glm::dot(B_[0], accel_)) / float(counter_acc_) + ((float(counter_acc_) - 1.0f) / float(counter_acc_)) * average_for_acceleration_;
	max_for_acceleration_ = std::max(abs(glm::dot(B_[0], accel_)), max_for_acceleration_);
	average_lat_acceleration_ = abs(glm::dot(B_[1], accel_)) / float(counter_acc_) + (float(counter_acc_) - 1.0f) / float(counter_acc_) * average_lat_acceleration_; 
	max_lat_acceleration_ = std::max(abs(glm::dot(B_[1], accel_)), max_lat_acceleration_);
	average_roll_rate_ = abs(roll_rate_) / float(counter_acc_) + ((float(counter_acc_) - 1.0f) / float(counter_acc_)) * average_roll_rate_;;
	max_roll_rate_ = std::max(abs(roll_rate_), max_for_acceleration_);
	average_out_of_bounds_ = float(position_.y < -20) / float(counter_acc_) +(float(counter_acc_) - 1.0f) / float(counter_acc_) * average_out_of_bounds_;

}


void CPrey::testSettings()
{
	if (!GetAsyncKeyState(VK_F11) && keyState_ != 0)
	{
		keyState_ = 0;
	}
	if (GetAsyncKeyState(VK_F11) && keyState_ == 0)
	{
		keyState_ = 1;
		SIM.PrintString(SIM.Params().evolution.fileName);
		SIM.PrintFloat(float(SIM.SimulationTime()), "Prey settings. Simulation Time");
		SIM.PrintString(pBird_.birdName);
		SIM.PrintFloat(float(pBird_.maneuver), "maneuver");
		SIM.PrintFloat(pBird_.wingMass, "wing mass");
		SIM.PrintFloat(pBird_.InertiaBody, "InertiaBody");
		SIM.PrintFloat(pBird_.J, "J");
		SIM.PrintFloat(pBird_.bodyMass, "bodymass");
		SIM.PrintFloat(pBird_.bodyArea, "bodyArea");
		SIM.PrintFloat(pBird_.cBody, "cBody");
		SIM.PrintFloat(pBird_.cFriction, "cFriction");
		SIM.PrintFloat(pBird_.cruiseSpeed, "cruiseSpeed");
		SIM.PrintFloat(pBird_.wingSpan, "wingSpan");
		SIM.PrintFloat(pBird_.wingBeatFreq, "wingBeatFreq");
		SIM.PrintFloat(pBird_.bodyWeight, "bodyWeight");
		SIM.PrintFloat(pBird_.rho, "rho");
		SIM.PrintFloat(pBird_.InertiaWing, "Inertia");
		SIM.PrintFloat(pBird_.speedControl, "speedControl");
		SIM.PrintVector(B_[0], "body x");
		SIM.PrintVector(B_[1], "body y");
		SIM.PrintVector(B_[2], "body z");
		SIM.PrintFloat(pPrey_.smoothManeuverParam.x, "smooth x");
		SIM.PrintFloat(pPrey_.smoothManeuverParam.y, "smooth y");
		SIM.PrintFloat(pPrey_.smoothManeuverParam.z, "smooth z");
		SIM.PrintFloat(pPrey_.smoothManeuverParam.w, "smooth w");
		SIM.PrintFloat(pPrey_.nonSmoothManeuverParam.x, "nonsmooth x");
		SIM.PrintFloat(pPrey_.nonSmoothManeuverParam.y, "nonsmooth y");
		SIM.PrintFloat(pPrey_.nonSmoothManeuverParam.z, "nonsmooth z");
		SIM.PrintFloat(pPrey_.nonSmoothManeuverParam.w, "nonsmooth w");
	}
		
}








