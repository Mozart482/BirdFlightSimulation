#include <string>
#include <exception>
#include <filesystem>
#include <glm/gtc/matrix_transform.hpp>
#include <glsl/buffer.hpp>
#include <glsl/texture.hpp>
#include <glsl/vertexarray.hpp>
#include <glsl/shader_pool.hpp>
#include "Globals.hpp"
#include "GLSLState.hpp"
#include "GLSLSkybox.hpp"
#include "GLSLiotexture.hpp"
#include "Params.hpp"


using namespace glsl;
namespace filesystem = std::tr2::sys;



GLSLSkybox::GLSLSkybox()
{
  auto MediaPath = filesystem::path(GGl.MediaPath());
  std::vector< std::string > FileNames;
  for (int i=0; i<6; ++i)
  {
    // 0, 90, 180, 270, up, down
    char buf[32] = {0};
    _snprintf_s(buf, 31, "%d.png", i);
    FileNames.push_back((MediaPath / "skybox" / PSKYBOX.name / buf).string());
  }
  CubeMap_ = LoadCubeMapTexture(FileNames, false);
  CubeMap_.set_wrap_filter(GL_CLAMP_TO_EDGE, GL_LINEAR, GL_LINEAR);
  GGl.use_program("SkyBox")->uniform("CubeMap")->set1i(3);
  CubeMap_.unbind();
  CubeMap_.bind(3);
  vao_.bind();
}


GLSLSkybox::~GLSLSkybox()
{
}


void GLSLSkybox::Flush()
{
}


void GLSLSkybox::Render()
{
  glsl::program* pprog = GGl.use_program("SkyBox");
  auto colorCorr = glm::vec4(PSKYBOX.ColorCorr, 1.0f);
  pprog->uniform("colorFact")->set4fv(1, glm::value_ptr(colorCorr));
  vao_.bind();
  glDepthFunc(GL_ALWAYS);
  glDrawArrays(GL_TRIANGLES, 0, 12*3);
  glDepthFunc(GL_LEQUAL);
  vao_.bind();
}


