#include "Globals.hpp"
#include "GLWin.hpp"


LuaStarDisplay Lua;
GLWin* AppWin = nullptr;
Simulation* Sim = nullptr;
std::atomic<bool> SimulationQuitFlag = false;


Globals::Globals()
{
  Lua.Open();
  AppWin = new GLWin();
  Sim = new Simulation();
}


Globals::~Globals()
{
  if (Sim) { delete Sim; Sim = nullptr; }
  if (AppWin) { delete AppWin; AppWin = nullptr; }
  Lua.Close();
}

