#include <exception>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <fstream>
#include <sstream>
#include "globals.hpp"
#include "filesystem.hpp"
#include "GLWin.hpp"
#include "Bird.hpp"
#include "Predator.hpp"
#include "Clock.hpp"
#include "Simulation.hpp"
#include "Camera.hpp"
#include "GLSLState.hpp"
#include "EvolvePN.hpp"
#include "KeyState.hpp"
#include "libLua.hpp"
#include "debug.hpp"
#include "random.hpp"
#include "Mmsystem.h"
#include "libParam.hpp"
#include <glmutils/random.hpp>
#include <glmutils/ray.hpp>


static GLubyte *pixels = NULL;

using namespace Param;


static const char header_fmt[] =
  "\n[F1] Help\n[F2] Prey: %d + %d\n[F3] Boundary radius: %d\n[F4] HRTree level: %d\nh.hildenbrandt@rug.nl\n";

static const char footer_fmt[] =
  "SIM. time: %02.0f:%02.0f:%02.0f\nupdate %.4f s\nfps: %d";


namespace {

  void printExceptionMsg(const char* prefix)
  {
    std::cout << (prefix ? prefix : "Unknown error") << ": ";
    auto luaErr = Lua.ErrMsg();
    std::cout << luaErr << '\n';
    debug::CppStackDump();
    std::cout << "\nBailing out\n";
  }

}


Simulation::Simulation()
: SimulationTime_(0.0),
  UpdateTime_(0.0),
  FrameTime_(0.1),
  statisticsPaused_(false),
  paused_(false),
  camera_(0),
  nextID_(1)
{
}


Simulation::~Simulation()
{
  // destroy before trail_buffer
  prey_.clear();
  pred_.clear();
}



void Simulation::SetParams(const Param::Params& param)
{
	params_ = param;
	

}

void Simulation::SetInitialParameter(const Param::Params& param)
{
  params_ = param;
	gl_.reset( new GLSLState() );
	gl_->Init(APPWIN.GetDC());
  trails_.reset( new trail_buffer_pool() );
  evolution.reset(nullptr);
}


void Simulation::GetExperimentSettings(const luabind::object& obj)
{

	luabind::object experiments = obj;

	for (luabind::iterator i(experiments), end; i != end; ++i)
	{
		auto Prey = prey_.begin();
		auto Pred = pred_.begin();
		Param::Experiment experiment;
		int intKey = luabind::object_cast<int>(i.key());
		experiment.preyBird = luabind::object_cast<Param::Bird>(obj[intKey]["preyBird"]);
		experiment.predBird = luabind::object_cast<Param::Bird>(obj[intKey]["predBird"]);
		experiment.pred = luabind::object_cast<Param::Predator>(obj[intKey]["pred"]);
		experiment.prey = luabind::object_cast<Param::Prey>(obj[intKey]["prey"]);
		experiment.param = libParam::FromLua<Param::Params>(obj[intKey]["Param"]);	
		SIM.experiments_.push_back(experiment);
	}
}


void Simulation::SetPRoost(const Param::Roost& roost)
{
  params_.roost.maxRadius = roost.maxRadius;
  params_.roost.minRadius = roost.minRadius;
  params_.roost.Radius = roost.Radius;
  for (auto& prey : prey_) { prey->RoostChanged(); };
  for (auto& pred : pred_) { pred->RoostChanged(); };
}


void Simulation::SetPRenderFlags(const Param::RenderFlags& flags)
{
  params_.renderFlags = flags;
}


void Simulation::RegisterFactories(const luabind::object& PreyFactory, const luabind::object& PredatorFactory)
{
  PreyFactory_ = PreyFactory;
  PredatorFactory_ = PredatorFactory;
}

void Simulation::RegisterDataStorage(const luabind::object& dataStorage)
{
	StorageData_ = dataStorage;
}

void Simulation::RegisterEvolution(const luabind::object& evolution_next)
{
	evolution_next_ = evolution_next;
}

luabind::object& Simulation::GetActiveCamera()
{ 
  return luaCamera_; 
}


void Simulation::SetActiveCamera(const luabind::object& luaobj)
{ 
  luaCamera_ = luaobj;
  camera_ = luabind::object_cast<ICamera*>(luaCamera_["cc"]);
}


void Simulation::PrintVector(glm::vec3 input, std::string text)
{

	std::cout << "\n" << text << " " << input.x << " " << input.y << " " << input.z;
}

void Simulation::PrintVec2(glm::vec2 input, std::string text)
{

	std::cout << "\n" << text << " " << input.x << " " << input.y << " ";
}

void Simulation::PrintFloat(float input, std::string text)
{

	std::cout << "\n" << text << " " << input;
}

void Simulation::PrintString(std::string text)
{

	std::cout << "\n" << text;
}

bool Simulation::HandleKey(unsigned key, unsigned ks)
{
  if (Lua.ProcessKeyboardHooks(key, ks))
  {
    return true;
  }
  bool handled = false;
  switch (key) 
  {
  case 'Z':
    {
      params_.realTime = !params_.realTime;
    }
  case 'M':  
    if (KEYSTATE_IS_CTRL(ks) || KEYSTATE_IS_SHIFT_CTRL(ks)) 
    {
      handled = true;
      unsigned& current = KEYSTATE_IS_SHIFT_CTRL(ks) ? gl_->currentPredatorModel() : gl_->currentPreyModel();
      if (++current >= params_.ModelSet.size()) current = 0;
      if (KEYSTATE_IS_SHIFT_CTRL(ks)) gl_->currentPredatorModel() = current;
      else gl_->currentPreyModel() = current;
      gl_->setAnnotation("3D model changed");
      gl_->LoadModels();
    } 
    break;
  case VK_UP:
	  //std::cout <<  "\nUp\n";//key up
  case VK_DOWN:
	  //std::cout << "\nUp\n";//key up
  case VK_RIGHT:
	  //std::cout << "\nUp\n";//key up
  case VK_LEFT:
	  //std::cout << "\nUp\n";//key up
  case VK_F2:
    if (KEYSTATE_IS_BLANK(ks) || KEYSTATE_IS_SHIFT(ks) || KEYSTATE_IS_SHIFT_CTRL(ks))
    {
      handled = true;
      bool shift = KEYSTATE_IS_SHIFT(ks) || KEYSTATE_IS_SHIFT_CTRL(ks);
      bool ctrl = KEYSTATE_IS_CTRL(ks) || KEYSTATE_IS_SHIFT_CTRL(ks);
      double n = static_cast<double>(prey_.size());
      n = n + (shift ? +1.0 : -1.0);
      int dn = (ctrl || (n <= 0.0)) ? 1 : static_cast<int>((::pow(10.0, ::floor(::log10(n)))));
      dn = glm::clamp(dn, 1, 1000);
      unsigned newNum = std::max(0, static_cast<int>(n + (shift ? +dn : -dn)));
      setNumPrey(newNum);
    }
    break;
  case VK_PAUSE: 
    handled = true;
    paused_ = !paused_; 
    gl_->setAnnotation(paused_ ? "Simulation paused" : ""); 
    break; 
  }
  return handled;
}


CBird* Simulation::PickNearestBird2Ray(const glm::vec3& ray_position, const glm::vec3& ray_direction)
{
  float mss = std::numeric_limits<float>::max();
  CBird* res = nullptr;
  for (const auto& p : prey_) {
    float ss = glmutils::distanceSqRayPoint(ray_position, ray_direction, p->position());
    if (ss < mss) {
      mss = ss;
      res = p.get();
    }
  }
  for (const auto& p : pred_) {
    float ss = glmutils::distanceSqRayPoint(ray_position, ray_direction, p->position());
    if (ss < mss) {
      mss = ss;
      res = p.get();
    }
  }
  return res;
}


CBird* Simulation::FindById(int id) const
{
  CBird* bird = nullptr;
  {
    auto it = std::find_if(prey_.begin(), prey_.end(), [=](const auto& b) { return b->id() == id; });
    if (it != prey_.end()) {
      bird = static_cast<CBird*>(it->get());
    }
  }
  if (bird == nullptr) {
    auto it = std::find_if(pred_.begin(), pred_.end(), [=](const auto& b) { return b->id() == id; });
    if (it != pred_.end()) {
      bird = static_cast<CBird*>(it->get());
    }
  }
  return bird;
}


CBird* Simulation::PickById(int id, bool showTrail)
{
  CBird* bird = FindById(id);
  if (bird) SetFocalBird(bird, showTrail);
  return bird;
}


void Simulation::SetPredatorTarget(const CBird* bird)
{
  CPredator* focal = const_cast<CPredator*>(camera_->GetFocalPredator());
  if (focal)
  {
    focal->SetTargetPrey(static_cast<const CPrey*>(bird));
  }
}


void Simulation::SetFocalBird(const CBird* bird, bool showTrail)
{
  if (0 == bird) return;
  camera_->SetFocalBird(bird);
  const_cast<CBird*>(bird)->setTrail(showTrail);
}


void Simulation::setNumPrey(unsigned newNum)
{
  newNum = std::min(params_.maxPrey, newNum);
  const CPrey* focalPrey = camera_->GetFocalPrey();
  int focalId = (focalPrey) ? focalPrey->id() : -1;
  if (newNum > prey_.size())
  {
    while (prey_.size() < newNum)
    {
      std::unique_ptr<CPrey> prey( luabind::object_cast<CPrey*>(PreyFactory_(nextID_++)) );
      prey_.emplace_back(std::move(prey));
    }
  } 
  else 
  {
    prey_.resize(newNum);
  }
  params_.roost.numPrey = newNum;
  auto it = std::find_if(prey_.cbegin(), prey_.cend(), [=](const auto& b) { return b->id() == focalId; }); 
  if (it == prey_.cend() || focalPrey != it->get())
  {
    focalPrey = newNum ? prey_.front().get() : nullptr;
  }
  camera_->SetFocalPrey(focalPrey);
  for (auto& prey : prey_) prey->NumPreyChanged();
  for (auto& pred : pred_) pred->NumPreyChanged();
}


void Simulation::setNumPredators(unsigned newNum)
{
  newNum = std::min(params_.maxPredators, newNum);
  const CPredator* focalPred = camera_->GetFocalPredator();
  int focalId = (focalPred) ? focalPred->id() : -1;
  if (newNum > pred_.size())
  {
    while (pred_.size() < newNum)
    {
      std::unique_ptr<CPredator> pred(luabind::object_cast<CPredator*>(PredatorFactory_(nextID_++)));
      pred_.emplace_back(std::move(pred));
    }
  }
  else
  {
    pred_.resize(newNum);
  }
  params_.roost.numPredators = newNum;
  auto it = std::find_if(pred_.cbegin(), pred_.cend(), [=](const auto& b) { return b->id() == focalId; });
  if (it == pred_.cend() || focalPred != it->get())
  {
    focalPred = newNum ? pred_.front().get() : nullptr;
  }
  camera_->SetFocalPredator(focalPred);
  for (auto& prey : prey_) prey->NumPredChanged();
  for (auto& pred : pred_) pred->NumPredChanged();
}


unsigned Simulation::getNumPrey() const
{ 
  return static_cast<unsigned>(prey_.size());
}


unsigned Simulation::getNumPredators() const
{ 
  return static_cast<unsigned>(pred_.size());
}


void Simulation::UpdateBirds(const float sim_dt)
{
  for (auto& prey : prey_) { prey->update(sim_dt); }
  for (auto& pred : pred_) { pred->update(sim_dt); }
}


void Simulation::next_experiment()
{
	expNumb_++;
	Generation_ = 0;
	if (expNumb_ > experiments_.size()) APPWIN.PostMessage(WM_CLOSE);
	auto firstPred = pred_.begin();
	auto lastPred = pred_.end();
	auto firstPrey = prey_.begin();
	auto lastPrey = prey_.end();

	CBird* bird = nullptr;
	bird = static_cast<CBird*>(firstPrey->get());

	SetFocalBird(bird, 0);
	luabind::globals(Lua)["cameraToTV"](luaCamera_);

	Param::Params p = experiments_[expNumb_ - 1].param;
	SetParams(p);
	for (; firstPred != lastPred; ++firstPred)
	{
		(*firstPred)->SetPredParams(experiments_[expNumb_ - 1].pred);
		(*firstPred)->SetBirdParams(experiments_[expNumb_ - 1].predBird);
	}
	for (; firstPrey != lastPrey; ++firstPrey)
	{
		(*firstPrey)->SetPreyParams(experiments_[expNumb_ - 1].prey);
		(*firstPrey)->SetBirdParams(experiments_[expNumb_ - 1].preyBird);
	}
	std::cout << "\n Experiment number: " << expNumb_;

	//save files of experiment
	time_t     now = time(0);
	struct tm  tstruct;
	char       buf[80];
	tstruct = *localtime(&now);
	strftime(buf, sizeof(buf), "%d-%m-%Y", &tstruct);


	std::string bufS(params_.DataStorage.folder);
	bufS.append(buf);
	bufS.append("/");
	bufS.append(params_.evolution.fileName);
	bufS.append("/");
	std::string luaName("experiment.lua");
	std::string fnameTrunc(std::string(params_.evolution.fileName.c_str()).substr(0, std::string(params_.evolution.fileName.c_str()).find(".txt")));


	CreateDirectory(bufS.c_str(), NULL);

	CopyFile("../../experiments.lua", (bufS + luaName).c_str(), TRUE);
	CopyFile((experiments_[expNumb_ - 1].param.birds.csv_file_prey_predator_settings).c_str(), (bufS +  "pred_prey.csv").c_str(), TRUE);
	CopyFile((experiments_[expNumb_ - 1].param.birds.csv_file_species).c_str(), (bufS + "species.csv").c_str(), TRUE);
}


void Simulation::Initialize_birds()
{
  auto firstPred = pred_.begin();
  auto lastPred = pred_.end();
  auto firstPrey = prey_.begin();
  auto lastPrey = prey_.end();
  
  float meanN = 0;
	float meanStartAltitude = 0;
	float meanXDist = 0;
	
	int N = static_cast<int>(pred_.size());
  int N_prey = static_cast<int>(prey_.size());
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> dis(0, N_prey-1);
	for (int nn = 0; nn < params_.evolution.Trajectories.amount && nn < N; ++nn)
	{
		(*(firstPred + nn))->pPred_.StoreTrajectory = true;

	};
	int counter = 0;
	for (; firstPred != lastPred; ++firstPred)
	{
		(*firstPred)->ResetHunt();
		(*firstPred)->setTrail(false);
		(*firstPred)->position_ = (*firstPred)->pBird_.InitialPosition;
		(*firstPred)->B_[0] = glmutils::save_normalize(-(*firstPred)->position_, glm::vec3(1, 0, 0));
		if (abs((*firstPred)->B_[0].y - 1) > 0.001)
		{
			(*firstPred)->B_[2]  = glm::normalize(glm::cross((*firstPred)->B_[0], glm::vec3(0, 1, 0)));
			glm::cross((*firstPred)->B_[2], (*firstPred)->B_[0]);
		}
		
		(*firstPred)->velocity_ = (*firstPred)->pBird_.InitialSpeed * (*firstPred)->B_[0];
		(*firstPred)->SetSpeed((*firstPred)->pBird_.InitialSpeed);
		(*firstPred)->setTrail(true);
		(*firstPred)->roll_rate_ = 0.0f;
		(*firstPred)->BeginHunt();
		(*firstPred)->r_prev_ = glm::vec3(0.0f, 0.0f, 0.0f);
		if (params_.evolution.oneOnOnePredPrey == false)
		{
      (*firstPred)->SetTargetPrey((firstPrey + dis(gen))->get());
		}
		else{
      (*firstPred)->SetTargetPrey((firstPrey + counter)->get());
			(*(firstPrey + counter))->associatedPredator_ = firstPred->get();
		}
		meanN += (*firstPred)->pPred_.N * 1.0f / float(N);
		meanStartAltitude += (*firstPred)->pBird_.InitialPosition.y * 1.0f / float(N);
		meanXDist += (*firstPred)->pBird_.InitialPosition.x * 1.0f / float(N);
		counter = counter + 1;
	};
  // store
	meanN_ = meanN;
	meanStartAltitude_ = meanStartAltitude;
	meanXDist_ = meanXDist;

	for (; firstPrey != lastPrey; ++firstPrey)
	{
		(*firstPrey)->setTrail(false);
		(*firstPrey)->position_ = (*firstPrey)->pBird_.InitialPosition;
		(*firstPrey)->B_[0] = (*firstPrey)->pBird_.InitialHeading;
		// reset the couunter to compute the averages
		(*firstPrey)->velocity_ = (*firstPrey)->pBird_.InitialSpeed * (*firstPrey)->B_[0];
		(*firstPrey)->SetSpeed((*firstPrey)->pBird_.InitialSpeed);
		(*firstPrey)->roll_rate_ = 0.0f;
		(*firstPrey)->set_counter_acc(0);
		(*firstPrey)->setTrail(true);
	};
}


void Simulation::UpdateSimulation(double sim_dt)
{
  if (!paused_) 
  {
    UpdateBirds(static_cast<float>(sim_dt));
	if (catch_ == 1 && Sim->getNumPredators() == 1)
	{
		gl_->setAnnotation("CATCH!");
		catch_ = 0;
		timeSinceEvolution = params_.evolution.durationGeneration - 2.0f;
	}
	if (catch_ == 2 && Sim->getNumPredators() == 1)
	{
		gl_->setAnnotation("MISS!");
		catch_ = 0;
		timeSinceEvolution = params_.evolution.durationGeneration - 2.0f;
	}
	  // specifically for evolution setting:
	  if (params_.evolution.type == "PN")
	  {
		  if (timeSinceEvolution > params_.evolution.durationGeneration)
		  {
			  if (expNumb_ == 0) next_experiment();
			  if (Generation_ >= params_.evolution.terminationGeneration) next_experiment();
			  //evolution->apply();
			  //evolution->save(params_.evolution.fileName.c_str(), 0); 
			  StorageData_(expNumb_);
			  Generation_ += 1;
			  evolution_next_(expNumb_);
			  Initialize_birds();
			  timeSinceEvolution = 0.0f;
		  }
	  }
	  // end evolution setting
  }
  luabind::globals(Lua)["CameraUpdateHook"](luaCamera_, sim_dt);
  camera_->Update(sim_dt);
}


void Simulation::OnSize(int height, int width)
{
  glm::ivec4 ClientRect(0, 0, width, height);
  glm::ivec4 Viewport(ClientRect);
  camera_->OnSize(Viewport, ClientRect);
  gl_->Resize();
}


void Simulation::OnLButtonDown(int x, int y, unsigned ks)
{
  if (KEYSTATE_IS_BLANK(ks) || KEYSTATE_IS_SHIFT(ks) || KEYSTATE_IS_CTRL(ks))
  {
  	Lua.ProcessMouseHooks(x, y, 0, false, ks);
  }
}


void Simulation::OnLButtonUp(int, int)
{
}


void Simulation::OnLButtonDblClk(int x, int y)
{
	Lua.ProcessMouseHooks(x, y, 0, true, KeyState());
}


void Simulation::OnMouseMove(int dx, int dy, bool LButtonPressed)
{
}


void Simulation::OnContextMenu(int menuEntry)
{
}


// Main simulation loop
void Simulation::EnterGameLoop()
{
  GlobalTimerRestart();
  const double dt = params_.IntegrationTimeStep;
  SimulationTime_ = 0.0;
  size_t SimulationTick = 0;
  double lastFrameDuration = 0.0;
  double lastRender = 0.0;
  double timeDrift = 0.0;
  bool done = 0;
  bool odd = true;
  int skippedFrame = 0;
  
  auto renderFun = [&] {
    if (odd) gl_->Flush(); else gl_->Render();
    odd = !odd;
    FrameTime_ = glm::mix(FrameTime_, GlobalTimerSinceReplace(lastRender), 0.005);
  };

  double frameBegin = GlobalTimerNow();
  luabind::object LuaTimeTickHook;
  for (;;)
  {
    // Serve message pump
    MSG msg;
    while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) != 0) 
    {
      if (msg.message == WM_QUIT)
      {
        return;
      }
      TranslateMessage(&msg);
      DispatchMessage(&msg);
      LuaTimeTickHook = luabind::globals(Lua)["TimeTickHook"];
    }

    // waste some time
    double udt = dt * (params_.renderFlags.slowMotion ? params_.slowMotion : 1);
    timeDrift = params_.realTime ? glm::mix(timeDrift, udt - lastFrameDuration, 0.1) : 0.0;
    double nowaitUpdate;
    while (timeDrift > GlobalTimerSinceCopy(frameBegin, nowaitUpdate)) 
    {
      if (params_.renderFlags.slowMotion) renderFun();  // super smooth slow motion
      std::this_thread::yield();                        // be nice
    }

    if (LuaTimeTickHook) LuaTimeTickHook(SimulationTime_, timeDrift);
    UpdateSimulation(dt);
    UpdateTime_ = glm::mix(UpdateTime_, GlobalTimerSince(nowaitUpdate), 0.01);

    if (!paused_)
    {
      ++SimulationTick;
      SimulationTime_ = SimulationTick * dt;
  	  timeSinceEvolution += dt;
    }
    else
    {
      std::this_thread::sleep_for(std::chrono::microseconds(params_.pausedSleep));
    }

    if ((timeDrift >= 0) || (skippedFrame == params_.maxSkippedFrames))
    {
			renderFun();
      skippedFrame = 0;
    }
    else
    {
      ++skippedFrame;
    }
    lastFrameDuration = GlobalTimerSinceCopy(nowaitUpdate, frameBegin);
	}
}


void Simulation::main(int argc, char** argv)
{
  liblua::Open((argc == 2) ? filesystem::path(argv[1]).string().c_str() : 0);
  if (!APPWIN.Create())
  {
    throw std::exception("Creating main window failed");
  }
  filesystem::create_directory(filesystem::path(luabind::object_cast<const char*>(LUASIM["DataPath"])));

  std::cout << "Starting simulation.\n";
  Lua.DoFile(luabind::object_cast<const char*>(LUASIM["ConfigFile"]));
  boost::optional<bool> quit = luabind::object_cast_nothrow<bool>(luabind::globals(LUA)["InitHook"]());
  if (quit) return;

  luabind::object rnd = luabind::globals(LUA)["random"];
  rnd_seed(luabind::object_cast<unsigned long>(rnd["getSeed"](rnd)));
  setNumPrey(params_.roost.numPrey);
  setNumPredators(params_.roost.numPredators);
  if (params_.renderFlags.turnOffGraphics) EnterGameLoopNoGraphicsNoLua(); else EnterGameLoop();
}


// Entry point
//
int main(int argc, char** argv)
{
  SimulationQuitFlag = false;
  std::locale::global(std::locale("C"));
  std::cout << "BFS\nInitialising simulation.\n";
  try 
  {
    Globals _;
    SIM.main(argc, argv);
  }
  catch (const char* w)
  {
    printExceptionMsg(w);
  }
  catch (const luabind::error& e)
  {
    printExceptionMsg(lua_tostring(e.state(), -1));
  }
  catch (const std::exception& e)
  {
    printExceptionMsg(e.what());
  }
  catch (...)
  {
    printExceptionMsg(0);
  }
  std::cout << "Regards\n";
  return 0;
}



void Simulation::EnterGameLoopNoGraphicsNoLua()
{
	GlobalTimerRestart();
	const double dt = params_.IntegrationTimeStep;
  size_t SimulationTick = 0;
	SimulationTime_ = 0.0;
	double prevTime = 0.0;
	double frameBegin = GlobalTimerNow();
	luabind::object LuaTimeTickHook;
	for (;;)
	{
		// Serve message pump
		MSG msg;
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) != 0)
		{
			if (msg.message == WM_QUIT)
			{
				return;
			}
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		UpdateSimulationNoGraphicsNoFlock(dt);
    ++SimulationTick;
		SimulationTime_ = SimulationTick * dt;
		if (prevTime < SimulationTime_)
		{
			PrintFloat(float(SimulationTime_), "time = ");
			prevTime += 1.0;
		}
		timeSinceEvolution += dt;
	}
}


void Simulation::UpdateSimulationNoGraphicsNoFlock(double sim_dt)
{
	
	UpdateBirds(static_cast<float>(sim_dt));
	// specifically for evolution setting:
	if (params_.evolution.type == "PN")
	{
		if (timeSinceEvolution > params_.evolution.durationGeneration)
		{
			if (expNumb_ == 0) next_experiment();
			if (Generation_ >= params_.evolution.terminationGeneration) next_experiment();
			//evolution.apply();
			//evolution.save(params_.evolution.fileName.c_str(), 0); 
			StorageData_(expNumb_);
			Generation_ += 1;
			evolution_next_(expNumb_);
			Initialize_birds();
			timeSinceEvolution = 0.0f;
		}
	}

}
