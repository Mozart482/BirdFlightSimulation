#ifndef GLSLSTATE_HPP_INCLUDED
#define GLSLSTATE_HPP_INCLUDED

#include <memory>
#include <array>
#include "glmfwd.hpp"
#include "Params.hpp"


namespace glsl { 
  class texture;
  class block_uniform;
  class uniform;
  class uniform_block;
  class shader_pool;
  class program;
  class buffer;
}


class GLSLState
{
  GLSLState(const GLSLState&);
  const GLSLState& operator=(const GLSLState&);

public:
	explicit GLSLState();
	~GLSLState();
	void Init(void* hDC);
  void Resize() const;
  void LoadModels();
  
  void Flush();
  void Render();

  glm::vec4 textColor() const;
  float alphaMaskCenter() const { return alphaMaskCenter_; }
  float alphaMaskWidth() const { return alphaMaskWidth_; }
  float& alphaMaskCenter() { return alphaMaskCenter_; }
  float& alphaMaskWidth() { return alphaMaskWidth_; }
  unsigned currentPreyModel() const { return currentPreyModel_; }
  unsigned& currentPreyModel() { return currentPreyModel_; }
  unsigned currentPredatorModel() const { return currentPredatorModel_; }
  unsigned& currentPredatorModel() { return currentPredatorModel_; }
  void setAnnotation(const char* str, double duration = 1.0);

  // Interface to shader pool
  glsl::program* use_program(const char* prog);
  std::string const& MediaPath() const { return MediaPath_; }

private:
  void LoadSkybox();
  void UploadMatrices();
  void UseSimViewport() const;
  void UseFullViewport() const;
  void PrintInfoText();

  std::unique_ptr<glsl::shader_pool>        ShaderPool;
  std::unique_ptr<class GLSLSkybox>         Skybox;
  std::unique_ptr<class glsl::texture>      SpectrumTex;
  std::unique_ptr<class GLSLBodyWingInstancingProg> InstancingPrey;
  std::unique_ptr<class GLSLBodyWingInstancingProg> InstancingPred;
  std::unique_ptr<class GLSLRibbonProg>     RibbonProg;
  std::unique_ptr<class glsl::buffer>       buBuf;

public:
  std::unique_ptr<class GLSLImm>            imm3D;
  std::unique_ptr<class GLSLImm>            imm2D;
  std::unique_ptr<class IText>              Fonts;

private:
  float alphaMaskCenter_;
  float alphaMaskWidth_;
  unsigned currentPreyModel_;
  unsigned currentPredatorModel_;
  std::string helpMsg_;
  std::string annotation_;
  double annotationElapsed_;
  Param::RenderFlags flags_;
  std::string MediaPath_;
	void* hDC_;
  void* hGLRC_;
};


#endif
